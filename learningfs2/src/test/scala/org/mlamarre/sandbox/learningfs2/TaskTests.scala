package org.mlamarre.sandbox
package learningfs2

import fs2.{ Scheduler, Strategy, Task }
import fs2.util.Async

import scala.concurrent.Future
import scala.concurrent.duration._
import java.util.concurrent.Executors

import org.scalatest.{ BeforeAndAfterAll, FunSuite, Matchers }

class TaskTests extends FunSuite with Matchers with BeforeAndAfterAll {

  private var testExecutorService = Executors.newFixedThreadPool(Runtime.getRuntime.availableProcessors * 2)
  private implicit var strategy: Strategy = Strategy.fromExecutor(testExecutorService)
  private implicit var schedulerExecutorService = Executors.newSingleThreadScheduledExecutor
  private implicit var scheduler: Scheduler = Scheduler.fromScheduledExecutorService(schedulerExecutorService)

  override def afterAll() = {
    super.afterAll()
    testExecutorService.shutdownNow
    schedulerExecutorService.shutdownNow
    testExecutorService = null
    strategy = null
    schedulerExecutorService = null
    scheduler = null
    ()
  }

  test("Using Task.Ref to synchronize mutable data") {
    // Task.Ref can be thought of as an analogue to AtomicReference, just more Task-y.

    object ThingManager {
      // This is the Ref constructor method, but it gives you back the Ref
      // wrapped in a Task. If you try to use the task as shown in the
      // commented out methods below, you will actually end up with TWO
      // DIFFERENT Refs when you call set/get! Thus, calls to getThing will
      // block forever, because the calls to putThing are updating a different
      // Ref object than the one that calls to getThing are waiting on.
      private val thing = Task.ref[Int]
      // def getThing: Task[Int] = thing.flatMap { _.get }
      // def putThing(data: Int): Task[Unit] = thing.flatMap { _.setPure(data) }

      // So you probably want to pop the Ref out of the Task for more typical use,
      // although this isn't really the most graceful way to do so for production
      // code
      private val thingRef = Task.ref[Int].unsafeRun
      def getThing: Task[Int] = thingRef.get
      def putThing(data: Int): Task[Unit] = thingRef.setPure(data)
    }

    // Two different tasks to set/get the Thing
    val t1 = ThingManager.getThing
    val t2 = ThingManager.putThing(5)

    // Run the putter task asynchronously after a delay
    t2.schedule(2 seconds).unsafeRunAsync(_ => ())

    // Time the getter task to show that `get` will block until the other
    // asynchronous task resolves and calls `set` (or `setPure`)
    val beforeTime = System.currentTimeMillis
    val result = t1.unsafeRun
    val afterTime = System.currentTimeMillis

    result shouldBe 5
    (afterTime - beforeTime) should be > 1500L
  }

  test("General Task.Ref examples") {
    // Again, you probably don't want to get at your Ref like this in production code
    val ref = Task.ref[Int].unsafeRun

    // Calls to get() and modify() will BLOCK if you have not previously called
    // set() or setPure()
    ref.setPure(23).unsafeRun

    ref.get.map { _ shouldBe 23 }.unsafeRun

    ref.modify(i => i + 1).map { change =>
      change.previous shouldBe 23
      change.now shouldBe 24
    }.unsafeRun

    ref.get.map { _ shouldBe 24 }
  }

  test("Initializing Task.Ref instances") {
    // This function creates a Ref that you can use as you'd expect
    def createGoodRef: Task.Ref[Int] = (for {
      ref <- Task.ref[Int]
      _ <- ref.setPure(0)
    } yield ref).unsafeRun

    // This function does not. Calls to get() on this Ref will block unless and until another
    // call is made to set() or setPure() on this ref.
    def createBadRef: Task.Ref[Int] = (for {
      ref <- Task.ref[Int]
      // NOTICE: We create a value here, we don't use a generator
      _ = ref.setPure(0)
    } yield ref).unsafeRun

    val goodRef = createGoodRef
    goodRef.get.unsafeRun shouldBe 0

    // Oops, this get call will block
    val badRef = createBadRef
    badRef.get.unsafeAttemptRunFor(2 seconds) should be('left)

    // But if you set the bad ref, it becomes usable
    badRef.setPure(2).unsafeRun
    badRef.get.unsafeRun shouldBe 2

    // Desugaring the first for comprehension is pretty straightforward. The work
    // gets chained through a simple set of tasks that are ultimately flattened out
    // into our final task that is run, yielding the properly initialized Ref.
    def desugaredGoodRef: Task.Ref[Int] = {
      Task.ref[Int].flatMap { ref =>
        ref.setPure(0).map(_ => ref)
      }.unsafeRun
    }

    // But the second one looks preeeeety weird, IMO. But, at least it reveals
    // what happened here. That value assignment from the setPure() call happens
    // in the first map() call. That generates a new Task that is stored in the
    // unused value. That value is then... not used. That is, the Task that would
    // set the Ref is never run. The second map() call discards that value and
    // just keeps the original value of `ref` that was created by the call to
    // Task.ref(). That is, this entire function body is equivalent to:
    // Task.ref[Int].unsafeRun
    def desugaredBadRef: Task.Ref[Int] = {
      Task.ref[Int].map { ref =>
        val unused = ref.setPure(0)
        (ref, unused)
      }.map { tup: (Task.Ref[Int], Task[Unit]) =>
        tup match {
          case (ref, _) => ref
        }
      }.unsafeRun
    }

    desugaredGoodRef.get.unsafeRun shouldBe 0
    desugaredBadRef.get.unsafeAttemptRunFor(2 seconds) should be('left)

    // The moral of this story is that you probably never want to create or
    // manipulate a Task like so in a for comprehension: `_ = <something Task-y>`
    // But `_ <- <something Task-y>` is probably safe.
  }

  test("Converting Future to Task") {
    import scala.concurrent.ExecutionContext.Implicits.global
    var callCount = 0

    // Starts a new Future that then executes its "logic"
    def somethingThatReturnsFuture(x: Int): Future[Int] = {
      callCount = callCount + 1
      Future(x * 2)
    }

    // NOTE: This code creates a Future, which immediately starts running. That Future
    // is then converted to a Task. But it is ALREADY RUNNING.
    val t1: Task[Int] = {
      val f = somethingThatReturnsFuture(3)
      // toTask is provided by our TaskEnrichedFuture class in the package object
      f.toTask
    }

    // Sleep for a moment
    java.lang.Thread.sleep(500)
    // The call count is updated even though the Task hasn't been run
    callCount shouldBe 1

    // Running the task just lets us get to the result. The Future inside of it
    // has already run, so the call count isn't updated again.
    val v1 = t1.unsafeRun
    v1 shouldBe 6
    callCount shouldBe 1

    // NOTE: This code creates a Task that is based on a Future. The Future inside
    // this Task will not run until the Task itself is run.
    val t2: Task[Int] = Task.fromFuture(somethingThatReturnsFuture(5))

    // Sleep for a moment
    java.lang.Thread.sleep(500)
    // The call count has not been updated since the second task has been created
    // but not yet run.
    callCount shouldBe 1

    // Once we run the second task, the call count will be updated to reflect the
    // fact that the underlying Future finally executed.
    val v2 = t2.unsafeRun
    v2 shouldBe 10
    callCount shouldBe 2
  }

}
