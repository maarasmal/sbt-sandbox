package org.mlamarre.sandbox

import fs2.Task

import scala.concurrent.{ Await, Future }
import scala.concurrent.duration._

package object learningfs2 {
  // A crude approximation of what a Future-to-Task typeclass might look like.
  implicit final class TaskEnrichedFuture[A](private val fut: Future[A]) extends AnyVal {
    def toTask: Task[A] = Task.delay(Await.result(fut, 2.seconds))
  }
}
