
# Michael's Sandbox

IMPORTANT: This project covers only Scala 2.x at the current time.

This is just a simple project that I've used over the years to capture bits of information that I
have learned about Scala over the years. I also use it for the occasional experiment to figure
out tricky situations.

The work is very fragmented and haphazard, so apologies for the organization (or lack thereof).
There are several things that were started and not finished, or stubs for things that I wanted to
explore but just never got around to.

So like... pardon our dust, and "reader beware" and whatnot.

# For Total Scala Newbies

There are a few files that are more polished and thorough and could be very helpful for newcomers
to Scala. In particular:

* [ForComprehensions.scala](https://bitbucket.org/maarasmal/sbt-sandbox/src/main/common/src/test/scala/org/mlamarre/sandbox/common/ForComprehensions.scala)
* [UsingEither.scala](https://bitbucket.org/maarasmal/sbt-sandbox/src/main/learningcats/src/test/scala/org/mlamarre/sandbox/learningcats/UsingEither.scala)
* [idiomatic-enums.scala](https://bitbucket.org/maarasmal/sbt-sandbox/src/main/common/src/test/scala/org/mlamarre/sandbox/common/idiomatic-enums.scala)
* [WtfIsATypeclass.scala](https://bitbucket.org/maarasmal/sbt-sandbox/src/main/common/src/test/scala/org/mlamarre/sandbox/common/WtfIsATypeclass.scala)

# Using the Code

You can run the tests with just `sbt test`. The tests should all pass by default. Note that some
tests are marked as "pending" and the real value of the test is the comments, or the explicit type
declarations in the code that demonstrate what various operations are doing.

