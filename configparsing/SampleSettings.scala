
package org.mlamarre
package foo

import java.util.UUID
import scala.concurrent.duration.FiniteDuration

case class SampleSettings(
  systemId: UUID,
  requestTimeout: FiniteDuration,
  monitoringInterval: FiniteDuration,
  bufferSize: Int
)

object SampleSettings {
  val Root = "org.mlamarre.foo.sample"

  implicit val uuidParser: DerivedConfigFieldParser[UUID] =
    ConfigParser.DerivedConfigFieldParser { key =>
      ConfigParser.fromString(key) { value =>
        try Right(UUID.fromString(value))
        catch { case _: IllegalArgumentException => Left(s"The value provided for the $key property ($value) is not a valid UUID") }
      }
    }

  implicit val configParser: ConfigParser[SampleSettings] = derived[SampleSettings].under(Root)

  implicit val sampleSettingsShow: Show[SampleSettings] = Show.show[SampleSettings] { settings =>
    s"""\nSample Settings:
         |system-id         = ${settings.systemId}
         | ...
         |buffer-size       = ${settings.bufferSize}""".stripMargin
  }

}

