# Random Notes

This is a collection of random snippets that I have stumbled upon, but not yet had time to write up
examples for.

## Use of parMapN
A:
I'm looking into using cats Parallel parMapN, but am getting the following error:
```
[error] /Users/jdoe000/vise/palermo-jdoe000-ssh/client/src/main/scala/com/comcast/palermo/client/SourceMultiplexOverview.scala:123:48: could not find implicit value for parameter p: cats.NonEmptyParallel[cats.effect.IO,F]
[error]                     Q2IPClient.getAll).parMapN { (_, _) =>
```
I'm currently looking through cats documentation to figure out what I'm missing but no luck yet
B:
 you need this implicit in scope: `implicit val cs: ContextShift[IO] =
     IO.contextShift(scala.concurrent.ExecutionContext.global)`
A:
 thanks! that did it
B:
 So… this is not obvious, but:

• You need `NonParallel[IO, F]`
• `Parallel` extends `NonParallel`
• You can get a `Parallel[IO, F]` via an implicit `ContextShift`

```
implicit def ioParallel(implicit cs: ContextShift[IO]): Parallel[IO, IO.Par] =
```

## Discussion about Prism

Two common applications of prisms:
1) Embedding one type inside another type -- e.g., Int inside String.  The smaller type is A and the larger (or equal cardinality) is S. Idea here is that every possible Int can be encoded as a String but not every String is an Int. More on this use case in a moment.
2) A common special case of use case 1 is a prism where S is an algebraic data type and A is one of the constructors of that data type. For example:
```sealed trait Status
case class Accepted(timestamp: Instant) extends Status
case class Rejected(reason: RejectionReason) extends Status
```
We can create two prisms here: `Prism[Status, Accepted]` and `Prism[Status, Rejected]`. In each case, we're saying that we can always go from a subtype to a supertype but given a value of the supertype, we can only go to the target subtype if the supertype is an instance of the subtype.
Re: use case 1, note that not every "embedding of a smaller type in to a larger type" can be represented by a Prism. For example, embeddings which "normalize" the value of the larger type are not prisms as they violate the second law I posted.
Example: consider a Color enumeration with values Red, Green, and Blue. Now consider two functions for converting a Color to/from a String.
```def colorToString(c: Color): String = c match {
  case Red => "Red"
  case Green => "Green"
  case Blue => "Blue"
}

def stringToColor(s: String): Option[Color] = s.trim.toLowerCase match {
  case "red" => Some(Red)
  case "green" => Some(Green)
  case "blue" => Some(Blue)
  case _ => None
}
```
(edited)
Do `colorToString` and `stringToColor` make a `Prism[String, Color]`?
No; proof by counterexample - take `s = "RED"` with the second law: `forAll { (s: S) => getOption(s).map(reverseGet).getOrElse(s) shouldBe s }`
`stringToColor("RED") == Some(Red)`
`Some(Red).map(colorToString) == Some("Red")`
`Some("Red").getOrElse(...) == "Red"`
But `"Red" != "RED"` (edited)
In this particular case, the `toLowerCase` normalization is the cause of our problem. The `trim` call is the cause of another problem. Removing both `trim` and `toLowerCase` would fix the issue.
