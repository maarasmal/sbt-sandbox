package org.mlamarre.sandbox
package learningcats

import cats.data.ValidatedNel
import cats.implicits._
import scala.concurrent.{ Await, ExecutionContext, Future }
import scala.concurrent.duration._
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

class ValidatedTests extends AnyFunSuite with Matchers {

  test("Learning Validated for fixing up old secure-cast code") {
    // Eric asked for my help with a couple of the thornier conversions when
    // upgrading our infrastructure code to a newer version of Scala. This also
    // included migrating Scalaz code to Cats. So this test case mimics roughly
    // the code in ProvisioningManagerActor that he couldn't figure out how to
    // update. The Future stuff is irrelevant as far as learning Validated, but
    // I included it so as to better mimic the secure-cast code I was trying to
    // update.

    // This stack overflow post was a little helpful when writing this test:
    // http://stackoverflow.com/questions/37773658/how-to-flatten-a-sequence-of-cats-validatednel-values

    case class Foo(x: Int, s: String)
    implicit val ec = ExecutionContext.global

    // Performs dummy validation of a Foo, possibly returns an error. Even though it is
    // a NEL, there's only going to be one String or one Foo here. We probably could have
    // just used Validated, but I didn't want to have to modify the secure-cast more than
    // necessary to just get it building again. This was not intended to be a redesign.
    def validateRequestedFoo(f: Foo): Future[ValidatedNel[String, Foo]] = {
      val result: Either[String, Foo] = if (f.x % 2 == 0)
        Right(f)
      else
        Left(s"Error: ${f.x}")
      Future.successful(result.toValidatedNel)
    }

    def getFoosToProvision(foos: List[Foo]): Future[Either[String, List[Foo]]] = {
      val fFoos: List[Future[ValidatedNel[String, Foo]]] = for {
        foo <- foos
      } yield validateRequestedFoo(foo)

      // Because we just want to gather up all of the valid Foos into a List, our solution here is much
      // simpler than the old Scalaz code: we just call sequenceU. More complicated solutions exist for
      // other needs (e.g., you have a bunch of Validated's for a bunch of different fields that you want
      // to use to construct a new object)
      val fValidation: Future[ValidatedNel[String, List[Foo]]] = (Future sequence fFoos).map(_.sequence)

      // In the secure-cast code, we just wanted to collapse everything and convert back to Either for simplicity.
      fValidation.map(_.leftMap(errs => s"Got the following error messages: ${errs.toList.mkString(",")}").toEither)
    }

    // First, validate some Foos that should all pass validation
    val legit = Await.result(getFoosToProvision(List(Foo(2, "mike"), Foo(4, "esmond"), Foo(6, "geoffrey"))), 2.seconds)
    legit should be('right)
    legit.foreach { foos => foos.length shouldBe 3 }

    // Validate some Foos where a couple of them fail validation
    val someFails = Await.result(getFoosToProvision(List(Foo(1, "draco"), Foo(4, "harry"), Foo(5, "goyle"))), 2.seconds)
    someFails should be('left)
    someFails.swap.foreach { errStr => errStr.endsWith("Error: 1,Error:5") }
  }
}
