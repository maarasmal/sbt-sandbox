package org.mlamarre.sandbox
package learningcats

import cats.implicits._
import cats.effect.IO
import cats.effect.unsafe.implicits.global

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

class TraverseTests extends AnyFunSuite with Matchers {
  test("Using traverse instead of map/sequence") {

    // The function `traverse` is equivalent to `map` followed by `sequence`. For instance, say you
    // have this function that takes an input and will create an IO that will do some "expensive"
    // calculation on the input and eventually evaluate to a result.
    def eventuallyDouble(input: Int): IO[Int] = IO(input * 2)

    // So if we have a set of inputs for which we want to perform this "expensive" calculation, we
    // might first do this.
    val l1: List[IO[Int]] = (1 to 10).toList.map(eventuallyDouble)

    // But we want to aggregate those results into a single IO. For that, we can use `sequence`,
    // which will essentially "swap" our two containers:
    val io1: IO[List[Int]] = (1 to 10).toList.map(eventuallyDouble).sequence

    // Unfortunately, using `map` followed by `sequence` means we end up iterating through our
    // collection twice. The function `traverse` will do the same thing as map followed by
    // sequence, but it only iterates through our List once:
    val io2: IO[List[Int]] = (1 to 10).toList.traverse(eventuallyDouble)

    io1.unsafeRunSync() shouldBe io2.unsafeRunSync()
  }

}
