package org.mlamarre.sandbox
package learningcats

import cats.data.EitherT
import cats.implicits._

import org.scalatest.BeforeAndAfterAll
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

/**
 * This class is adapted from an older sandbox class named UsingDisjunctions.
 * That class covered the use of Scalaz's \/ type. With the change in Scala 2.12
 * to make the standard Either type right-biased, Cats dropped it's Xor type in
 * favor of the standard library type. Cats still provides a number of enrichments
 * though, to add functionality to the standard Either type that brings it in
 * line with much of the functionality offered by \/. The Left and Right subtypes of
 * Either were represented by -\/ and \/- in Scalaz, respectively.
 *
 * @author mlamarre
 * @since JDK 1.6
 */
class UsingEither extends AnyFunSuite with Matchers {

  // Convenience class for the tests below. When this class was first written for \/, we frequently
  // used a type called Err on the left that held more than just this String value, but adding this
  // here reduced how many changes I needed to make below when updating the tests
  case class Err(msg: String)

  test("Converting an Option to a disjunction") {
    // Some standard Scala methods return Options, but you may find it useful to convert them
    // to Eithers. Scalaz provided toRightDisjunction() enrichment to Option, but Cats inverts
    // the perspective by adding fromOption() to the Either object via enrichment.
    val someOpt = Option("foo")
    val noneOpt = Option.empty[String]

    assert(someOpt.isDefined)
    assert(!noneOpt.isDefined)

    // fromOption() will convert an Option to an Either. If the Option is a Some[T],
    // the value of with type T will be put on the right side of the resulting Either.
    // If it is a None, the default value passed to fromOption will be placed on the
    // left side of the resulting Either.
    val someDj = Either.fromOption(someOpt, "Oops, your Option was a None")
    val noneDj = Either.fromOption(noneOpt, "Oops, your Option was a None")

    assert(someDj.isRight)
    assert(noneDj.isLeft)

    // Cats also provides toRight(), which works like the old Scalaz method
    assert(someOpt.toRight("Oops").isRight)
    assert(noneOpt.toRight("Oops").isLeft)

    // Scalaz provided a \/> "operator" is a synonym for toRightDisjunction(). Cats does provide
    // some unusual symbolic operators for certain operations, but they are less common than in
    // Scalaz.

    // If you find yourself using a match-case statement to create a disjunction from an
    // Option, fromOption() and toRight() are much more compact equivalents. The
    // following two functions do the exact same thing.
    def conversionWithMatchCase(opt: Option[Int]): Either[String, Int] =
      opt match {
        case None => Left("You had a None!")
        case Some(s) => Right(s)
      }

    def conversionWithFunction(opt: Option[Int]) = Either.fromOption(opt, "You had a None!")

    assert(conversionWithMatchCase(Option(23)).isRight)
    assert(conversionWithFunction(Option(24)).isRight)
    assert(conversionWithMatchCase(Option.empty[Int]).isLeft)
    assert(conversionWithFunction(Option.empty[Int]).isLeft)

    // There is also a toLeftDisjunction method in Scalaz. This works inversely to toRightDisjunction.
    // That is, if you call it on a Some[T], you will get a -\/ with your T on the left side. If you
    // call it on a None, you will get a \/- with the value that you passed to toLeftDisjunction on the
    // right side.
    // Cats provides a similar toLeft() method for Option.
    someOpt.toLeft("right value") shouldBe Left("foo")
    noneOpt.toLeft("right value") shouldBe Right("right value")
  }

  test("Using preconditions with Eithers") {
    // When using catchNonFatal(), you can put require() calls either inside or outside
    // of the catchNonFatal() call. Putting them inside will cause the error to be
    // caught on the left side of your Either.
    def methodWithPreconditions(value: Int): Either[String, Int] = Either
      .catchNonFatal {
        require(value > 0, "value must be non-negative")
        value * 2
      }
      .leftMap(t => t.getLocalizedMessage)

    assert(methodWithPreconditions(2).isRight)
    assert(methodWithPreconditions(-1).isLeft)
  }

  test("Eithers work well in for comprehensions") {
    // Scalaz provided handy 'left' and 'right' methods that enriched basically anything,
    // so you could say 23.right[String] to create a String \/ Int. From what I can tell,
    // Cats' equivalent isn't quite as syntactically pleasant. You have to explicitly
    // list both types of the Either. If you just say Either.right(23), you end up with
    // a Either[Nothing, Int].
    val rightOne = Either.right[String, Int](1)
    val rightTwo: Either[String, Int] = Either.right(23)
    val leftOne = Either.left[String, Int]("Oops!")

    // Using Either as a generator in a for comprehension is a simple way to extract the
    // right side of a disjunction.
    val sum = for {
      firstVal <- rightOne
      secondVal <- rightTwo
    } yield firstVal + secondVal

    assert(sum.isRight)
    assert(sum.getOrElse(-1) == (rightOne.getOrElse(0) + rightTwo.getOrElse(0)))

    // Recall that for-comprehensions are implemented as calls to flatMap, map and filter. As of
    // Scalaz 2.12, these functions are right-biased on Either, meaning they are essentially no-ops
    // if the Either is a Left. This essentially allows you to exit out of a for-comprehension as
    // soon as you hit an error. The resulting value will contain the left value that caused the
    // early exit.
    val ERR_ONE = "First error"
    val ERR_TWO = "Second error"
    val okGen = Either.right(1)
    val badGen1 = Either.left(ERR_ONE)
    val badGen2 = Either.left(ERR_TWO)

    val result = for {
      v1 <- okGen
      _ = v1 should be(1)
      v2 <- badGen1
      v3 <- badGen2
    } yield v1 + v2 + v3

    result should be('left)
    result.swap.map(_ should be(ERR_ONE))

    // But what if your 'yield' statement is the thing that generates a Left? Answer: Your 'yield'
    // shouldn't result in an Either, or you're going to have a nested Either (see the next test
    // for how to cleanly handle those when they are unavoidable)
    val res2 = for {
      v1 <- okGen
    } yield badGen1

    // The above statement does not "early out" with the Left that is badGen1. You instead end up with
    // a nested Either. The outer one is a Right, the inner one is a Left and has the error string from
    // badGen1
    res2 should be('right)
    res2.foreach { inner =>
      inner should be('left)
      inner.swap.foreach(_ should be(ERR_ONE))
    }

    // This is probably more like what you want to do to cleanly capture the error.
    val res3 = for {
      v1 <- okGen
      v2 <- badGen1
    } yield v2
    res3 should be('left)
    res3.swap.foreach(_ should be(ERR_ONE))
  }

  test("Disjunctions within disjunctions") {
    // You may at times end up with a value that has a type that looks essentially like
    // Either[A, Either[A,B]]. You probably want/need that to actually be an Either[A,B],
    // and you don't want to lose either error value, if present. This is a little like the
    // 'flatten' method on collections, but there is no such method on Either.
    // You can mimic it simply using the flatMap method.

    // Try this with nested Right values
    val inner1 = Right(123)
    val outer1 = Right(inner1)
    outer1 should be('right)
    outer1.foreach { value =>
      value should be('right)
      value.foreach(_ should be(123))
    }
    val flattened1 = outer1.flatMap(value => value)
    flattened1 should be('right)
    flattened1.foreach(_ should be(123))

    // Try again with a Right that contains a Left as its right-side value
    val inner2 = Left("Inner Err")
    val outer2 = Right(inner2)
    outer2 should be('right)
    outer2.foreach { value =>
      value should be('left)
      value.swap.foreach(_ should be("Inner Err"))
    }
    // You can also use the 'identity' function, which might be a little more clear.
    val flattened2 = outer2.flatMap(identity)
    flattened2 should be('left)
    flattened2.swap.foreach(_ should be("Inner Err"))

    // No inner object when we've already got an Err on the left of the outer Either.
    val outer3 = Either.left[String, Either[String, Int]]("Outer Err")
    outer3 should be('left)
    outer3.swap.foreach(_ should be("Outer Err"))
    val flattened3 = outer3.flatMap(value => value)
    flattened3 should be('left)
    flattened3.swap.foreach(_ should be("Outer Err"))
  }

  test("Using Eithers in test code") {
    // Simple 'should' matching with an Either
    val leftDj = Left("some error message")
    leftDj should be('left)
    leftDj shouldBe 'left
    leftDj should be(Left("some error message"))

    // 'should' matching on Eithers in a Map.
    val myMap: Map[String, Either[Err, String]] =
      Map(("failkey" -> Left(Err("some error message"))), ("successkey" -> Right("hooray!")))
    myMap.size should be(2)
    myMap should contain("failkey" -> Left(Err("some error message")))
    myMap should contain("successkey" -> Right("hooray!"))

    // Although exceptions are not common in Scala, you may sometimes end up with an
    // Either[Throwable, A] when working with Java libraries, and you want to verify that a
    // particular exception was thrown. Below are a couple patterns for doing that.
    val fakeResult: Either[Exception, Nothing] = Left(new IllegalArgumentException("hello"))

    fakeResult match {
      case Left(t) => t shouldBe an[IllegalArgumentException]
      case Right(whatever) => fail("I was expecting an exception")
    }

    fakeResult.leftMap(_ shouldBe an[IllegalArgumentException])
  }

  test("Using EitherT to access the innards of an Either") {
    // Frequently in the code that I worked with at CCAD/Comcast, I'd have to operate on values
    // that look something like IO[Either[Err, T]]. Trying to access that T value for subsequent
    // operations can be cumbersome, since you have to map() the IO, then map() the Either as well.
    // This also makes it hard to use in for-comprehensions. Cats provides a type called EitherT
    // that allows you to reach inside of Either values that reside within some other container
    // to easily extract the T value that you wish to manipulate, but still preserves the outer
    // container type. Thus you can easily transform an IO[Either[Err, T]] into an
    // IO[Either[Err, U]] with a single map() call.

    // This test is going to make use of cats-effect's IO class, but we aren't going to get into
    // that here. As of this writing, I don't have any dedicated tests that examine IO, so for now
    // the best I can do is refer you to their own docs: https://typelevel.org/cats-effect/docs/getting-started

    import cats.effect.IO
    import cats.effect.unsafe.implicits.global

    val ioEither: IO[Either[Err, String]] = IO(Right("abc"))
    val etIo: EitherT[IO, Err, String] = EitherT(ioEither).map(_.toUpperCase())

    // To get the IO back out of the EitherT, we call value:
    val theIo: IO[Either[Err, String]] = etIo.value

    // IO is lazy by default, so we still have to "run" the IO to examine the effects of the
    // statements above.
    val result: Either[Err, String] = theIo.unsafeRunSync()
    result.isRight shouldBe true
    result.foreach(_ shouldBe "ABC")

    // Here's a more complicated, although still contrived, example. We want to search a List for a
    // value that is derived from one of the values in the list. For example, imagine a scenario
    // where you search a List[T], but just want to return a single field from the T that you find.
    // The wrinkle is that the List is wrapped in an IO and an Either.
    case class Foo(id: Int, name: String)
    val ioFoos: IO[Either[Err, List[Foo]]] = IO {
      Right(List(Foo(1, "mike"), Foo(2, "grace"), Foo(3, "esmond")))
    }

    val etIoName: EitherT[IO, Err, String] = for {
      // Use EitherT to extract the right side of the Either that is inside the container wrapped
      // by EitherT (in this case, that container is an IO)
      foos <- EitherT(ioFoos)
      theFoo = foos.find(_.id == 2)
      // Now that we've found our item (of type Option[Foo]), we want to change its type from
      // Option to Either, then we wrap it in EitherT(IO(...)) so that our generator matches the
      // first one of this for-comprehension
      res <- EitherT(IO(theFoo.toRight(Err("The specified ID (2)could not be found"))))
    } yield res.name

    // Now our IO[Either[Err, List[Foo]]] has been mapped to an IO[Either[Err, String]] like we
    // wanted. Use `value` to pop the IO out of the EitherT, then run the IO to verify the result.
    val ioName: IO[Either[Err, String]] = etIoName.value
    val maybeName: Either[Err, String] = ioName.unsafeRunSync()
    maybeName.isRight shouldBe true
    maybeName.foreach(_ shouldBe "grace")
  }

  test(
    """What happens if a generator returns a Left, but you are using '_' to capture the generated value?"""
  ) {
    def returnRight(x: String) = Right[Err, String](x)
    def returnLeft(str: String): Either[Err, String] = Left(Err(str))

    val result = for {
      r1 <- returnRight("hello")
      _ <- returnLeft("oops")
      r2 <- returnRight("world")
    } yield r1 + r2

    // The error in the Left is preserved!
    result should be('left)
    result.swap.map(e => e should be(Err("oops")))
  }

  test("""Different types on either side of Either generators""") {
    def stringRight(s: String): Either[Err, String] = Right(s)
    def intRight(i: Int): Either[Err, Int] = Right(i)

    // Obviously succeeds when first generator and final yielded type are identical.
    val result = for {
      r1 <- stringRight("hello") // Generates Either[Err, String]
      r2 <- intRight(123) // Generates Either[Err, Int]
    } yield r1 + r2 // Yield a String
    result should be('right)
    result.map(r => r should be("hello123"))

    // The de-sugared form
    stringRight("hello").flatMap { r1 => intRight(123).map { r2 => r1 + r2 } }

    // Can also change the right side of the Either (here, from String to Int)
    val other = for {
      r1 <- stringRight("hello") // Generates Either[Err, String]
      r2 <- intRight(123) // Generates Either[Err, Int]
    } yield r2 // Yield an Int
    other should be('right)
    other.map(r => r should be(123))

    // But what if we start w/ Either[Err, Int] and end up with Either[String, Int]? We're going to
    // early-out before the 'yield' because the second generator returns a Left. But the compiler
    // makes the result type Object because the two generators have different left-side types.
    /* TODO: The code below does not match the comment above */
    def intRightString(i: Int): Either[Err, Int] = Right(i)
    def stringLeft(str: String): Either[String, Int] = Left(str)
    val oops = for {
      r1 <- intRightString(345)
      r2 <- stringLeft(
              "Not cool... left side of the Either doesn't match that of the first generator"
            )
    } yield r1 + r2
    oops should be('left)
    oops.swap.map(l => l shouldBe a[String])
  }

  test("Using guards with Eithers in for-comprehensions") {
    // Short version: DON'T (probably)
    // Long version: Guards translate to calls to withFilter/filter. For Either, the filter method
    // needs  a Monoid for the left side of your value so that it can build a Left from M.zero if
    // the value on the right doesn't satisfy the filtering predicate. In theory this is fine, but
    // in practice, you are typically going to have something like Err or Exception on the left of
    // your Either. There is no Monoid defined for these classes (how do you define the
    // zero-exception?), so when you add a guard condition in a for-comprehension with Either
    // generators, you will get an error if you do not have an implicit Monoid in scope for the
    // type on the left hand side type of your Either.
  }

  test("Sequencing collections that contain disjunctions") {
    // Start with a List[Either[String, Int]]
    val a1 = (1 to 10).toList.map(x => Either.right[String, Int](x))

    // The sequence() method then inverts the Either and the List to make it an
    // Either[String, List[Int]]
    val sequenced = a1.sequence
    sequenced should be('right)
    sequenced.foreach(intlist => intlist should have size 10)

    // Calling sequence() again should undo the transformation.
    val reseq: List[Either[String, Int]] = sequenced.sequence
    reseq.foreach(dj => dj should be('right))
    reseq should contain theSameElementsInOrderAs (a1)

    // If any of the items in a collection of Eithers is a left, the result of sequence will also
    // be a left.
    val a2: List[Either[String, Int]] = List(Right(1), Right(2), Right(4), Left("ERROR"), Right(99))
    val sequencedA2 = a2.sequence
    sequencedA2 should be('left)

    // Sequencing back-to-back should yield the original collection.
    val resequenced = a1.sequence.sequence
    resequenced should contain theSameElementsInOrderAs (a1)

    // NOTE: If your container has a Left, you will lose the rest of the data in your container once
    // you sequence it, so resequencing gives you back the right type, but not necessarily the same
    // data.
    val hasErr: List[Either[String, Int]] = List(Right(1), Right(2), Left("ERROR"), Right(99))
    val errResequenced: List[Either[String, Int]] = hasErr.sequence.sequence
    errResequenced.head should be('left)
    errResequenced.size should not equal hasErr.size
  }

  test("""Converting Option[Either[A, B]] to Either[A, Option[B]]""") {
    // Esmond asked how to do the transformation shown in the test description, and it turns out that
    // sequence is again the answer. As soon as I tried to write this test and saw the one about
    // sequencing above, I realized that Option is a container, just like List, so sequence should
    // do what I want.

    // First, try with an option of a Right
    val rOpt: Option[Either[String, Int]] = Option(Right[String, Int](5))
    val sequencedRight: Either[String, Option[Int]] = rOpt.sequence
    sequencedRight should be('right)

    // Then try with an option of a Left
    val lOpt: Option[Either[String, Int]] = Option(Left[String, Int]("ERROR"))
    val sequencedLeft: Either[String, Option[Int]] = lOpt.sequence
    sequencedLeft should be('left)
  }

  test("""Logging errors in a Left""") {
    // At times, you may have an Either for which you want to log an error, but leave the Either as
    // it is (instead of stripping off the outer container just to get the error value), either to
    // return it or because you need to return a value that is not an Either. For example, say
    // you have a method that is retrieving a value, and if the retrieval fails, you end up with
    // an Either, but you're just going to use some default value in that case but you would like
    // to log that an error occurred before returning the default value.

    // A dummy debug message method for calling in the examples below
    def writeDebug(s: String): Unit = ()

    // Scenario 1: You can use leftMap and just discard/ignore the new Either that it builds you.
    // This is sort of a code smell, since you are running leftMap() solely to get the side effect
    // of writing the debug message. Acceptable in a test, perhaps, but probably not in production
    // code.
    val vValue = Left[Exception, Int](new Exception("Error string"))
    vValue leftMap { t => writeDebug(t.getLocalizedMessage) }

    // Option 2: Swap the Either types/values and call foreach, but again discard the new Either
    // that is created by swap. This makes it a bit more clear that you are "doing nothing", since
    // foreach returns Unit, but isn't really any faster, and takes a tiny bit more code.
    vValue.swap.foreach(t => writeDebug(t.getLocalizedMessage))

  }

  test("Mapping a Either w/ a tuple inside") {
    // The question came up about how to call map() on a Either whose right side was a tuple.
    val x = Either.right[String, Tuple2[Int, Int]]((1, 2))

    // You can write a multiline map() call like so:
    // val y = x.map { stuff =>
    //   val (foo, bar) = stuff
    //   ...  // Process the tuple elements
    // }
    // But if you only need to do one thing w/ the tuple's elements, it might be simpler if you
    // could somehow extract them as part of the function you pass to map(). A partial function
    // is what you want:
    val y = x.map { case (a, b) => a + b }

    y.foreach(_ shouldBe 3)
  }

  test("String to byte conversion") {
    // Esmond asked me about some code he was writing to convert user input to validated Byte
    // values. This test isn't super Either-related, but I was working in the REPL at first, and
    // when I switched Option() to \/.fromTryCatch (now Either.catchNonFatal), I had to get out of
    // the REPL and move the code here, and I was going to try to fold() on the Either to get the
    // value out. Then I remembered that Either also supports getOrElse, which reads much simpler
    // in this case

    // These 3 functions are what I came up with as a cleaner alternative to the if/else and
    // try/catch-laden chunk of code that Esmond initially sent me.
    def isNumericString(s: String) = !s.isEmpty && s.forall(_.isDigit)
    def strToBoundedByte(s: String, min: Byte, max: Byte): Byte = {
      // Note: s.toByte can throw an exception
      Either.catchNonFatal(s.toByte).map {
        case x if x < min => min
        case x if x > max => max
        case x => x
      } getOrElse min
    }
    def convertInput(s: String): Byte = if (isNumericString(s)) strToBoundedByte(s, 0, 60) else 0

    // Another implementation that gets rid of the if/else statement, although I'm not sure this
    // is more readable
    def convertTwo(s: String): Byte =
      Option(s).filter(isNumericString).map(x => strToBoundedByte(x, 0, 60)).getOrElse(0)

    // I originally tried to replace the if/else w/ a collectFirst call instead of filter, but I
    // was having issues. I eventually figured it out though (I was forgetting the 'case' part of
    // the partial function):
    def convertThree(s: String): Byte =
      Option(s)
        .collectFirst { case x: String if isNumericString(x) => x }
        .map(x => strToBoundedByte(x, 0, 60))
        .getOrElse(0)

    convertInput("hi") shouldBe (0)
    convertInput("0") shouldBe (0)
    convertInput("23") shouldBe (23)
    convertInput("mike loves scala") shouldBe (0)
    convertInput("100000") shouldBe (0)

    convertTwo("hi") shouldBe (convertInput("hi"))
    convertTwo("23") shouldBe (convertInput("23"))
    convertTwo("1000000") shouldBe (convertInput("1000000"))
  }

  test("Swapping Lefts for clean error logging") {
    // Rather than using an 'if' statement with 'isLeft', you can simply swap/foreach the Either.
    val msg = "Some error message"
    val lefty = Either.left[String, Int](msg)
    var leftCheck = false
    lefty.swap.foreach { err =>
      err should be(msg)
      leftCheck = true
    }
    leftCheck should be(true)

    // If the Either is not a Left, the same statement structure is basically a no-op
    val righty = Either.right[String, Int](23)
    var rightCheck = false
    righty.swap.foreach { err => rightCheck = true }
    rightCheck should be(false)
  }

  test("Silly epiphany on nested Eithers and for comprehensions") {

    // Just a fake function that happens to return an Either
    def doStuff(x: Int): Either[String, Int] = Right(x + 1)

    // This isn't really a brilliant point, but I had a small realization about nested Eithers.
    // Sometimes they are the result of a for comprehension that is written like so:
    val foo = Either.right[String, Int](1)
    val nested: Either[String, Either[String, Int]] = for {
      // extract the value from the disjunction
      value <- foo
      // use the extracted value in a function that takes a normal parameter, but returns Either
    } yield doStuff(value)

    // But now you have a nested disjunction, which is cumbersome (see test case above)
    nested.foreach(inner => inner.isRight shouldBe true)

    // The simple, stupid trick is to add one more step to your for comprehension
    val flat: Either[String, Int] = for {
      // extract the value from the disjunction
      value <- foo
      // do the stuff, but extract the result from the return value of doStuff()
      result <- doStuff(value)
      // 'result' is now just a value, not an Either, so yield that, and it will get wrapped back
      // up into an Either
    } yield result

    flat.foreach(inner => inner shouldBe (foo.toOption.get + 1))
  }

}
