package org.mlamarre.sandbox
package learningcats

import cats.data.OptionT
import com.comcast.ip4s.Ipv4Address

import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.should._

class UsingOptionT extends AnyWordSpec with Matchers {

  test("Handy OptionT features") {
    // Alan came to me with a question about how to call a series of functions that look like this:
    def matchVendorA(ipv4Addr: Ipv4Address): F[Option[String]] = ???
    def matchVendorB(ipv4Addr: Ipv4Address): F[Option[String]] = ???
    def matchVendorC(ipv4Addr: Ipv4Address): F[Option[String]] = ???
    def matchVendorD(ipv4Addr: Ipv4Address): F[Option[String]] = ???

    // He wanted to write a function that would call those 4 functions in turn. If the first one
    // returned a Some, then return that value, otherwise call the next one in turn and so on. But
    // the F's that each function returns muddy the waters. He had a large for comprehension that
    // worked but was somewhat cumbersome. My mind immediately went to Option's orElse function,
    // but that requires you to pass it an Option[B], not an F[Option[B]]. Then I tried looking at
    // OptionT to see if it might provide some handy way to work with the Options inside of F's.
    // As it turns out, it has a version of orElse called orElseF that perfectly handles this
    // situation.

    def getVendorForIp(ipAddr: Ipv4Address): F[Option[String]] =
      OptionT(matchVendorA(ipAddr))
        .orElseF(matchVendorB(ipAddr))
        .orElseF(matchVendorC(ipAddr))
        .orElseF(matchVendorD(ipAddr))
        .value

    // In Alan's case, his return type for getVendorForIp was slightly different, but that simply
    // called for a final .map(...) to be added to the end of the statement after .value.

    // Since I don't want to write implementations for the functions above, I'm just going to cheat
    // and make this test always pass.
    assert(true)
  }
}
