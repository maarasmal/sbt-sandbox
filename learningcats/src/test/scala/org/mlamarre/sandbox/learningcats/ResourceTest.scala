package org.mlamarre.sandbox
package learningcats

import cats.effect.IO
import cats.effect.kernel.Resource
import cats.implicits._

import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.should._

import scala.util.Random

import cats.effect.unsafe.implicits.global

/**
 * Created this class to hold a snippet of thought that I wanted to try out at some point to
 * explore more about Resource.
 *
 * NOTE: As it currently stands, this is SUPER unlikely to compile... sigh, this whole repo is so out of date... :(
 */
class ResourceTest extends AnyWordSpec with Matchers {

  def makeServerResource: Resource[IO, String] = ???
  def httpClient: Resource[IO, String] = ???

  "cats-effect Resources" should {

    "placeholder" in {
      pending

      // TODO: this is scrap work for a scenario that GC asked about, where he had a test that
      // he wanted the server started fresh for each test case, and also needed an HTTP client
      // too. Based on https://typelevel.org/cats-effect/datatypes/resource.html, I think it should
      // be possible to compose the two resources together and run them and have them both get
      // cleaned up automagically.
      val r = for {
        svr <- makeServerResource // generates your Resource[IO, Server[IO]]
        client <- httpClient // generates your Resource[IO, Client[IO]]
      } yield (svr, client)

      r.use {
        case (s, c) =>
          ??? // Mostly I think you'd use `c` here? Send the request and process the response?
      }
        .unsafeRunSync() // Start everything running

      // TODO: This stuff is just straight copied from https://typelevel.org/cats-effect/datatypes/resource.html
      val acquire: IO[String] = IO(println("Acquire cats...")) *> IO("cats")
      // acquire: IO[String] = Bind(
      //   Delay(<function0>),
      //   cats.FlatMap$$Lambda$7477/1979472212@5fb2e1eb
      // )
      val release: String => IO[Unit] = _ => IO(println("...release everything"))
      // release: String => IO[Unit] = <function1>
      val addDogs: String => IO[String] =
        x => IO(println("...more animals...")) *> IO.pure(x ++ " and dogs")
      // addDogs: String => IO[String] = <function1>
      val report: String => IO[String] =
        x => IO(println("...produce weather report...")) *> IO("It's raining " ++ x)
      // report: String => IO[String] = <function1>

      Resource.make(acquire)(release).evalMap(addDogs).use(report).unsafeRunSync

    }
  }
}
