package org.mlamarre.sandbox
package learningcats

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._
import scala.util.Random
import cats.free.Trampoline
import cats.data.EitherT
import cats.implicits._
import Trampoline._

class TrampolineTests extends AnyFunSuite with Matchers {

  test("Porting CastAgentActor code from Scalaz to Cats") {

    // Dummy function to mock up something like getJsonString from CastAgentActor. That function
    // was originally implemented w/ Scalaz Trampolines, but Eric is trying to convert it to use
    // Cats. This is a mock re-implementation to see how close we can get to the original using
    // Cats.
    // Instead of reading cast-agent output, we merely read random integers.
    def readRandoms(maxReads: Int = 100): Either[String, Int] = {
      val StopValues = (1 to 10).toVector

      def readR(numRead: Int): Trampoline[Either[String, Int]] = {
        if (numRead >= maxReads) done(Right(numRead))
        else {
          // Read another random value
          val newRando = Random.nextInt(100)

          // If we generated 0, treat that as an "error" just so we can exercise that case
          val vInt: Either[String, Int] = if (newRando == 0) {
            println(s"We read a zero after $numRead reads")
            Left("ERROR")
          } else Right(newRando)

          val et = for {
            s <- EitherT(vInt.pure[Trampoline])
            r <- EitherT(defer(readR(numRead + 1)))
          } yield r

          et.value
        }
      }

      readR(0).run
    }

    val result = readRandoms(100)

    println(s"Result of reading randoms: $result")
  }

  test("Classic even/odd example") {
    pending
  }
}
