lazy val CatsVersion = "2.6.1"
lazy val CatsEffectVersion = "3.3.7"
lazy val CirceVersion = "0.14.2"
lazy val Fs2Version = "3.7.0"
lazy val Ip4sVersion = "3.2.0"
lazy val MUnitVersion = "1.0.0-M6"
lazy val ScalaTestVersion = "3.2.10"
lazy val ScalaTestScalaCheckVersion = "3.2.2.0"
lazy val ScalaCheckVersion = "1.14.3"
lazy val SshjVersion = "0.34.0"

lazy val commonSettings = Seq(
  organization := "com.mlamarre",
  version := "1.0.0-SNAPSHOT",
  scalaVersion := "2.13.12",
  libraryDependencies ++= Seq(
    "org.scalatest" %% "scalatest" % ScalaTestVersion % "test",
    "org.typelevel" %% "cats-core" % CatsVersion,
    "org.typelevel" %% "cats-kernel" % CatsVersion,
    "org.typelevel" %% "cats-free" % CatsVersion,
    "org.typelevel" %% "cats-effect" % CatsEffectVersion,
    "co.fs2" %% "fs2-core" % Fs2Version,
    "co.fs2" %% "fs2-io" % Fs2Version,
    "com.comcast" %% "ip4s-core" % Ip4sVersion,
    "org.scalatestplus" %% "scalacheck-1-14" % ScalaTestScalaCheckVersion % "test",
    "org.scalacheck" %% "scalacheck" % ScalaCheckVersion % "test",
    "org.scalameta" %% "munit" % MUnitVersion % Test,
    "org.scalameta" %% "munit-scalacheck" % MUnitVersion % Test
  ) ++ Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % CirceVersion)
)

lazy val root = project
  .in(file("."))
  .settings(commonSettings)
  .settings(
    name := "mlamarre-sandbox-parent"
  )
  .aggregate(
    common
    // learningcats,
    // learningfs2,
    // fs2intro,
    // fs2Summer2017,
    // learningscalacheck
  )

lazy val common = project
  .settings(commonSettings)
  .settings(
    name := "common",
    libraryDependencies ++= Seq(
      "com.hierynomus" % "sshj" % SshjVersion
    )
  )

// lazy val learningcats = project
//   .settings(commonSettings)
//   .settings(
//     name := "learningcats"
//   )

// lazy val learningfs2 = project.settings(commonSettings).settings(
//   name := "learningfs2",
//   libraryDependencies ++= Seq(
//     "co.fs2" %% "fs2-core" % "0.9.5",
//     "co.fs2" %% "fs2-io" % "0.9.5",
//     "co.fs2" %% "fs2-cats" % "0.3.0"
//   )
// )

// lazy val fs2intro = project.settings(commonSettings).settings(
//   name := "fs2intro",
//   libraryDependencies ++= Seq(
//     "co.fs2" %% "fs2-core" % "0.9.5",
//     "co.fs2" %% "fs2-io" % "0.9.5",
//     "co.fs2" %% "fs2-cats" % "0.3.0"
//   )
// )

// lazy val learningscalacheck = project
//   .settings(commonSettings)
//   .settings(
//     name := "learningscalacheck"
//   )

// lazy val fs2Summer2017 = project.settings(commonSettings).settings(
//   name := "fs2Summer2017",
//   libraryDependencies ++= Seq(
//     "org.typelevel" %% "cats-effect" % "0.3",
//     "co.fs2" %% "fs2-core" % "0.10.0-M4",
//     "co.fs2" %% "fs2-io" % "0.10.0-M4",
//     "co.fs2" %% "fs2-cats" % "0.3.0"
//   )
// )
