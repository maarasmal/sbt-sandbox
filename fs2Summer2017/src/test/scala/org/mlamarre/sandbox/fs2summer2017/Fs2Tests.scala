package org.mlamarre.sandbox
package fs2summer2017

import cats.effect._

import fs2._
// import fs2.{ Scheduler, Strategy }
// import fs2.util.Async
//
// import scala.concurrent.Future
// import scala.concurrent.duration._
// import java.util.concurrent.Executors

import org.scalatest.{ FunSuite, Matchers }

class Fs2Tests extends FunSuite with Matchers {

  test("Placeholder test") {
    val s: Stream[IO, Int] = Stream.eval(IO {
      println("IO is running")
      23
    })

    s.runLog.unsafeRunSync shouldBe Vector(23)
  }
}
