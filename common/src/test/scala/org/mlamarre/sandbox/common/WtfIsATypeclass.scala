package org.mlamarre.sandbox.common

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

/**
 * Demonstrates the core ideas behind a typeclass and relates them to the Functor typeclass
 * introduced by Mike P. during the 2/13/15 Scala Friday session. The discussion of typeclasses
 * here is pretty crude and focuses more on implementation than on philosophical reasons for WHY
 * you might use this design pattern. For more details (including great visuals to demonstrate the
 * decoupling that typeclasses allow for), this video is a bit dull, but very informative and easy
 * to follow: http://youtu.be/sVMES4RZF-8.
 */
class WtfIsATypeclass extends AnyFunSuite with Matchers {

  test("Phase 1 - Subtype polymorphism") {
    // Suppose we have a method that we want to use to process a Thing.
    sealed trait Thing {
      def doStuff: String
    }
    def processThing(x: Thing): String = x.doStuff

    // Fairly straightforward, right? In Java, anything that we'd want to pass to this method
    // would have to implement the interface Thing. This is known as "subtype polymorphism".
    case class Foo(name: String, value: Int) extends Thing {
      override def doStuff: String = s"${name}: ${value.toString}"
    }

    case class Bar(value: Double) extends Thing {
      override def doStuff: String = s"Bar: ${value.toString}"
    }

    processThing(Foo("harry", 1)) should be("harry: 1")
    processThing(Bar(3.0)) should be("Bar: 3.0")
  }

  test("Phase 2 - Ad-hoc polymorphism, Java style") {
    // The approach in the test method above requires that every possible value passed to
    // processThing implements the Thing interface. This may not be possible (if the object
    // that we want to process comes from a third party library) or desirable (forcing something
    // to implement the Thing trait/interface can introduce an unnecessary coupling between
    // modules). This test method shows how we can decouple the Foo and Bar classes from the
    // Thing trait. Although the code here is Scala, it should be easy to see how this could
    // be implemented with Java interfaces. Here is our Thing trait, unchanged from before:
    sealed trait Thing {
      def doStuff: String
    }

    // So lets localize the coupling to a separate interface that converts any class A into a
    // Thing. ThingConverter is what is known as a "typeclass". This is basically the same idea as
    // the Adapter pattern from the Gang of Four book.
    trait ThingConverter[A] {
      def makeThing(a: A): Thing
    }

    // This lets us make our processThing method a little more generic. It can now take
    // any object A as long as we also provide an object to convert the first object to
    // a Thing. The coupling between A and Thing is now localized to just the implementation
    // of ThingConverter, leaving the original class A untouched and pure.
    def processThing[A](a: A, conv: ThingConverter[A]): String = conv.makeThing(a).doStuff

    // We implement a ThingConverter for each class that we want to pass to processThing().
    // Notice that Foo and Bar no longer need to be concerned with Thing at all, decoupling them
    // from Things entirely. They can remain focused on their own domains.
    case class Foo(name: String, value: Int)
    object FooConverter extends ThingConverter[Foo] {
      override def makeThing(a: Foo) = new Thing {
        override def doStuff: String = s"${a.name}: ${a.value}"
      }
    }

    case class Bar(value: Double)
    object BarConverter extends ThingConverter[Bar] {
      override def makeThing(a: Bar) = new Thing {
        override def doStuff: String = s"Bar: ${a.value.toString}"
      }
    }

    // We pas our "pure" Foo and Bar objects, along with their converters. But this is more verbose
    // than the calls we made with subtype polymorphism. This is where Scala has some handy
    // features to help clean this up, which we will cover in the next test case.
    processThing(Foo("ron", 1), FooConverter) should be("ron: 1")
    processThing(Bar(3.0), BarConverter) should be("Bar: 3.0")
  }

  test("Phase 3 - Ad-hoc polymorphism made cooler by Scala") {
    // The above approach works just fine, but Scala implicits allow for a more convenient syntax
    // for working with typeclasses. Here again is our familiar Thing trait:
    sealed trait Thing {
      def doStuff: String
    }

    // This is the same ThingConverter trait that we had above:
    trait ThingConverter[A] {
      def makeThing(a: A): Thing
    }

    // This is the same processThing() method as above, except that we curry the ThingConverter
    // to a separate parameter group. We also allow the converter to be provided implicitly.
    def processThing[A](a: A)(implicit conv: ThingConverter[A]): String = conv.makeThing(a).doStuff

    // This is the same Foo and FooConverter as before, but the converter is now made available
    // implicitly.
    case class Foo(name: String, value: Int)
    implicit object FooConverter extends ThingConverter[Foo] {
      override def makeThing(a: Foo) = new Thing {
        override def doStuff: String = s"${a.name}: ${a.value}"
      }
    }

    // We give the Bar class a similar treatment.
    case class Bar(value: Double)
    implicit object BarConverter extends ThingConverter[Bar] {
      override def makeThing(a: Bar) = new Thing {
        override def doStuff: String = s"Bar: ${a.value.toString}"
      }
    }

    // Now we can call processThing() without having to explicitly mention the converter that
    // transforms a Foo or a Bar into a Thing. These calls look identical to the ones above when
    // we used subtype polymorphism.
    processThing(Foo("hermione", 1)) should be("hermione: 1")
    processThing(Bar(3.0)) should be("Bar: 3.0")
  }

  test("Functor is one of many typeclasses") {
    // The Functor trait that Mike P. discussed during Scala Friday on 2/13/15 is just another
    // typeclass, like ThingConverter (above). Here is the Functor trait that he defined during his
    // talk (I omitted the @typeclass annotation he added later, as well as the derived functions
    // like lift() and compose() for simplicity. The Functor defined in Cats is more powerful than
    // this demonstration version):
    trait Functor[F[_]] {
      def map[A, B](fa: F[A])(f: A => B): F[B]
    }

    // This allows us to write methods that wish to perform a mapping operation in terms of a
    // Functor rather than some specific type that implements a map() method. The signature of
    // doMapping() below is a tiny bit more complicated than that of processThing() from the
    // previous test, but it has essentially the same "shape" as processThing(). It takes in a
    // single value and then uses a typeclass (in this case, Functor) to manipulate that object
    // (in this case, by mapping the value in 't' from an A to a B).
    def doMapping[T[_], A, B](t: T[A])(f: A => B)(implicit F: Functor[T]): T[B] = F.map(t)(f)

    // We can then use doMapping() with any class that can has a Functor defined, whether that
    // class defines an explicit map() method or not. Here, Widget does not define a map() method,
    // but we can implement a Functor for Widget so that we can use Widget as if it DID have a
    // map() method.
    case class Widget[A](name: String, value: A)
    implicit object WidgetFunctor extends Functor[Widget] {
      override def map[A, B](fa: Widget[A])(f: A => B): Widget[B] = Widget(fa.name, f(fa.value))
    }
    doMapping(Widget("voldemort", 66))(x => x / 2) should be(Widget("voldemort", 33))

    // Here's a Functor for the standard Option class, which does define a map() method. We will
    // use that to implement the Functor. Cats provides Functor instances for many common classes
    // like Option and Either (again, much more powerful than this demonstration version).
    implicit object OptionFunctor extends Functor[Option] {
      override def map[A, B](fa: Option[A])(f: A => B): Option[B] = fa.map(f)
    }
    doMapping(Option("snape"))(x => s"${x}:${x}") should be(Option("snape:snape"))
  }

  test("Context bounds - alternate syntax for typeclasses") {
    // This trait defines a minimum contract for what the 'foo' method below will want to do with
    // an 'A'.
    trait Fooable[A] {
      def getValue(a: A): Int
    }

    implicit object StrFooable extends Fooable[String] {
      override def getValue(a: String): Int = {
        if (a.forall(c => c.isDigit)) a.toInt else 0
      }
    }

    implicit object IntFooable extends Fooable[Int] {
      override def getValue(a: Int) = a
    }

    // This syntax for the generic parameter is syntactic sugar for our usual implicit converter
    // type. This method is equivalent to "def foo[A](a: A)(implicit fooable: Fooable[A]): Int"
    def foo[A: Fooable](a: A): Int = {
      val fooable = implicitly[Fooable[A]]
      fooable.getValue(a) + 1
    }

    foo("123") should be(124)
    foo("abc") should be(1)
    foo(12) should be(13)
  }
}
