package org.mlamarre.sandbox.common

import cats.implicits._
import cats.effect._
import cats.effect.unsafe.implicits.global
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

/**
 * Experiments using cats-effect's Resource class
 */
class ResourceTests extends AnyFunSuite with Matchers {

  case class Foo(id: Int, name: String) {
    def initClean(): Unit = ()
    def initThrow(): Unit = throw new Exception("Exceptions suck!")
    def cleanup(): Unit = ()
  }

  test("Creating a Resource where the acquire step can throw exceptions") {
    // Create a Foo and then initialize it. The initialization method can throw
    val create: IO[Foo] = IO.delay {
      val x = Foo(1, "grace")
      x.initThrow()
      x
    }

    // Create the resource. Since we are using IO, the Foo is not actually created and initialized
    // at this point.
    val res = Resource.make(create)(foo => IO(foo.cleanup()))

    // Now we want to go use the resource. But we know that it can throw during acquisition, so we
    // call attempt() on it before calling use().
    val program: IO[Int] = res.attempt.use{ maybeFoo =>
      maybeFoo.isLeft shouldBe true
      println("MAL - inside program's use()")
      maybeFoo.fold(IO.raiseError, foo => IO.delay(foo.id))
    }

    // Run the program. At this point. the IO.raiseError above will result in the exception being
    // propagated. We have to use Either.catchNonFatal() here to properly handle it. In a typical
    // application, if we didn't know to call catchNonFatal(), this exception could crash the
    // entire application, so we maybe don't want to handle the exception this way...
    println("MAL - running program")
    val result: Either[Throwable, Int] = Either.catchNonFatal(program.unsafeRunSync())
    result.isLeft shouldBe true

    // If we just catch and log the error, are we cool then?
    val program2: IO[Option[Int]] = res.attempt.use{ maybeFoo =>
      maybeFoo.isLeft shouldBe true
      println("MAL - inside program2's use()")
      // Well, for one, if we just want to log the error and move on, we can't fold() the way we
      // did before, since we need matching types for the two cases...
      // maybeFoo.fold(IO.println("caught and logged error"), foo => IO.delay(foo))

      // And if we don't have a good "default" Foo to return here, what should we do? Maybe just
      // wrap our result type in an Option instead? In the case of some sort of network connection
      // resource, we'd probably map on the Either here, make our network calls inside of map(),
      // calculate our result value and then call toOption().
      IO(maybeFoo.map(_.id).toOption)
    }

    println("MAL - Running program2")
    val result2: Option[Int] = program2.unsafeRunSync()
    result2.isEmpty shouldBe true
  }

  test("Does the release happen if the acquire throws?") {
    // This test cases mocks up a sample that came up while trying to use sshj. You create the SSH
    // object, then connect(), then authenticate(), all in the acquire phase of the Resource. But
    // if we connected but failed to authenticate, the resource is never "acquired", so the release
    // step is never called, resulting in eventual timeouts where the SSH connections are severed
    // minutes later with some ugly error messages. So how should we handle possible errors during
    // resource acquisition?

    // We will throw an exception during acquire to force our scenario
    val acquire = IO.delay{
      val x = Foo(2, "mike")
      x.initThrow()
      x
    }

    // A flag to tell us whether the release function was ever called
    var releaseCalled = false

    // Set the flag to indicate release ran
    val release = { foo: Foo =>
      IO{
        releaseCalled = true
        foo.cleanup()
      }
    }

    val res: Resource[IO, Foo] = Resource.make(acquire)(release)
    val program = res.attempt.use{ maybeFoo =>
      maybeFoo.isLeft shouldBe true
      IO(maybeFoo.map(_.id).toOption)
    }

    // Observe that even though the code is safely runnable, the release was never called because
    // of the exception in the acquire. If we just called use(), we'd get the exception thrown.
    // But since we call attempt() first, the call to use() will always produce a "safe" result. But
    // apparently if the acquire doesn't complete successfully, release is never run.
    val result = program.unsafeRunSync()
    result.isDefined shouldBe false
    releaseCalled shouldBe false

    // So what if we skip attempt() and catch our own exceptions in the acquire? We make the
    // "subject" type of the Resource an Either, and the acquire will always "succeed", thus the
    // release will always be called, right?

    val acquire2: IO[Either[Throwable, Foo]] = IO{
      val y = Foo(3, "r2-d2")
      Either.catchNonFatal{
        y.initThrow()
        y
      }
    }

    val release2 = { maybeFoo: Either[Throwable, Foo] =>
      IO{
        releaseCalled = true
        maybeFoo.map(_.cleanup())
      }.void
    }

    val res2: Resource[IO, Either[Throwable, Foo]] = Resource.make(acquire2)(release2)
    // NOTE: we omit attempt() here, but the body of use() is identical to the previous one.
    val program2 = res2.use{ maybeFoo =>
      maybeFoo.isLeft shouldBe true
      IO(maybeFoo.map(_.id).toOption)
    }

    // NOW, we see that releaseCalled is set as expected when program2 is run
    releaseCalled = false
    val result2 = program2.unsafeRunSync()
    result2.isDefined shouldBe false
    releaseCalled shouldBe true
  }
}