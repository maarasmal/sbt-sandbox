package org.mlamarre.sandbox.common

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

class HigherOrderFunctions extends AnyFunSuite with Matchers {

  /**
   * This test is as much about "lazily evaluated" function parameters as it is about
   * composing higher order functions (HOFs), but I had a case in CastAgentActor where
   * I wanted to add a HOF that would have to take another HOF as a parameter. The
   * "inner" HOF had a complicated parameter list that I did not want to try to tackle
   * listing in the spec for the "outer" HOF. So I wanted to see if I could pass the
   * "inner" function such that it was lazily evaluated, which greatly simplifies the
   * method signature of the "outer" HOF. This test was to prove out the mechanism.
   */
  test("Composing HOFs") {
    case class FooType(data: String)

    var callCount = 0;

    // Both our inner and outer HOFs return FooType. In CastAgentActor, this was because
    // some times I wanted to use just the inner HOF, but sometimes I wanted to wrap it
    // with the outer HOF. Here, the type of "inner" can be thought of as "a function that
    // takes no arguments and returns a FooType". But we only call that function if we
    // need to.
    def outerHof(callInner: Boolean, outerData: String)(inner: => FooType): FooType = {
      if (callInner) inner else FooType(outerData)
    }

    def innerHof(str: String): FooType = {
      callCount += 1
      FooType(str)
    }

    // In these calls, the value passed to the "inner" parameter of outerHof() can be thought
    // of as a function that takes no arguments, calls innerHof() and returns the value that
    // is returned by innerHof(). In that way, innerHof() can be seen to be "lazily" called
    // only when this anonymous function is called by outerHoff().
    outerHof(true, "ned")(innerHof("tyrion")) should be(FooType("tyrion"))
    outerHof(false, "cat")(innerHof("tyrion")) should be(FooType("cat"))
    outerHof(false, "robb")(innerHof("tyrion")) should be(FooType("robb"))
    outerHof(false, "arya")(innerHof("tyrion")) should be(FooType("arya"))
    callCount should be(1)
  }

  test("by name parameters vs. Function0 parameters") {
    // This test case demonstrates some practical differences between functions that take by-name parameters and
    // functions that take Function0 (no-arg) parameters. The code here is adapted from the discussion that I had
    // with Mike P. over HipChat about these differences.

    // This is a function that takes a 'by name' parameter of type Boolean. That is, the parameter 'f' is a
    // lazily evaluated Boolean value.
    def foo(f: => Boolean): Int = if (f && f) 1 else 0

    // This function, on the other hand, takes a no-args function that returns a Boolean value. In this case, 'f' is
    // an object that extends Function0[Boolean]. As you can see, the fact that 'f' is a function requires us to add
    // empty parens when we call it in the body of our method.
    def bar(f: () => Boolean): Int = if (f() && f()) 1 else 0

    // Both types of parameters behave similarly, with the most significant difference usually being just the syntax
    // required both in the function and when calling the function. For instance, in both cases, the passed value is
    // evaluated each time it is referenced in the body of the method.
    var fooCount = 0
    foo({ fooCount = fooCount + 1; true })
    fooCount should be(2)
    var barCount = 0
    bar(() => { barCount = barCount + 1; true })
    barCount should be(2)

    // You can also see above the syntactical differences required to invoke 'foo' and 'bar'. The latter requires the
    // prefix "() =>". Syntactically, by-names are typically a little nicer on the user of your code, although they
    // cannot be used as members of classes.
    foo(true)
    bar(() => true)
    // Does not compile: case class Stuff(x: => Int)
    case class Things(y: () => Int)

    // According to Mike P., using a lazy val in the body of your method that takes a 'by name' parameter is a
    // common idiom to only use the value of 'f' once, but still delay its execution, if necessary.
    def baz(f: => Boolean): Int = { lazy val ff = f; ff; ff; ff; 0 }
    var bazCount = 0
    baz { bazCount = bazCount + 1; true }
    bazCount should be(1)

    // One last comment borrowed from my discussion with Mike P. regarding this distinction between parameter types:
    // "by-names are only similar to *applied* function0s. Function0s can be manipulated in lots of other ways though"
  }

  test("You can't just make everything lazy") {
    // Lazy evaluation is cool, but you don't want to go using it everywhere. Consider the following:
    // lazy firstName = // read console input
    // lazy lastName = // read console input
    // trace(s"Read the name: ${lastName}, ${firstName}")
    succeed
  }
}