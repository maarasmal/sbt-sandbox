package org.mlamarre.sandbox.common

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

/**
 * Demonstrates uses of pattern matching in Scala. Note that while some of these examples may use
 * regular expressions, we are using the term pattern matching to refer to things like match-case
 * statements, where you match against a "pattern" of an object, typically a case class.
 */
class PatternMatching extends AnyFunSuite with Matchers {

  test("Pattern matching arrays, with nested pattern matches") {

    val versionReleaseRegex = """([0-9]+)\.([0-9]+)\.([0-9]+)[-](.*)$""".r
    val versionRegex = """([0-9]+)\.([0-9]+)\.([0-9]+)$""".r

    def doMatch(s: String): Either[String, String] = s.split(" ") match {
      // So here, we see we don't need to convert an Array to a List just to do
      // pattern matching. Also, we nest a pattern match using a regular expression
      // inside of the pattern match of the Array. Lastly, note that we are
      // able to name the entire value matched by the regex still, even though it
      // is nested inside of the Array pattern match.
      case Array(_, vers @ versionReleaseRegex(_, _, _, _), _*) => Right(vers)
      case Array(_, vers @ versionRegex(_, _, _), _*) => Right((vers + "-1"))
      case _ => Left(s"Unable to determine version from: ${s}")
    }

    doMatch("foo-pkg 3.0.1") should be(Right("3.0.1-1"))
    doMatch("foo-pkg 3.0.7-1") should be(Right("3.0.7-1"))
    doMatch("foo-pkg 3.0.5 blah") should be(Right("3.0.5-1"))
    doMatch("foo-pkg").isLeft shouldBe true
    doMatch("foo-pkg 3.1-R1").isLeft shouldBe true
    doMatch("foo-pkg 3.1").isLeft shouldBe true
    doMatch("foo-pkg 3.1.2.4").isLeft shouldBe true
  }

  test("Matching multiple case classes with the same case") {
    sealed trait Foo
    trait Message { val s: String }
    case class Mike(s: String) extends Foo with Message
    case class Esmond(s: String) extends Foo with Message
    case class Eric(s: String) extends Foo with Message

    val x = Mike("hello")
    val y = Esmond("hola")
    val z = Eric("hi")

    def getStr(f: Foo): String = f match {
      case e: Eric => "huh"
      // TODO: This comment doesn't match the code below. Look into improving this example and
      // giving a better description here.
      // You can't say "case Mike(s) | Esmond(s)", you have to use this "@" notation. In this case,
      // I also had to add a type for 's' to make the
      case c: Message => c.s
      case _ => "I dunno man"
    }

    getStr(x) should be(x.s)
    getStr(y) should be(y.s)
    // getStr(z) should be("I dunno man")
  }
}
