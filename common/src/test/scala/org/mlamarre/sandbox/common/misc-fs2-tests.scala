package org.mlamarre
package sandbox
package common

import cats.implicits._
import cats.effect._
import cats.effect.unsafe.implicits.global
import fs2._
import org.scalatest.BeforeAndAfterAll
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._
import scala.concurrent.duration._

class MiscFs2Tests extends AnyFunSuite with Matchers with BeforeAndAfterAll {

  test("Streams stop producing values after an error is raised") {
    // Produce 3 values, then an error, then a 4th value
    val errStream = Stream(1,2,3).covary[IO] ++
      Stream.raiseError[IO](new Exception("error occurred")) ++
      Stream.eval(IO(4))

    // Run the stream, turn any error into the value 0. Notice that after we handle the error,
    // the value 4 is never produced by the stream, since it comes after the error was generated.
    val result = errStream.handleErrorWith(t => Stream(0)).compile.toList.unsafeRunSync()
    result shouldBe List(1,2,3,0)

    // Stream#attempt works similar to handleErrorWith. attempt() generates each value wrapped in
    // Either, with successful values being Right until the first error is encountered, which will
    // be a Left.
    val result2 = errStream.attempt.compile.toList.unsafeRunSync()
    result2.size shouldBe 4
    result2.drop(3).head.isLeft shouldBe true
  }

  test("Use bracket() to automatically acquire and clean up resources") {
    pending
  }

  // This test is an attempt to document how one can set up an fs2 Stream to run continuously
  // at some interval, possibly with multiple workers performing work. It was modelled after one
  // of our existing polling streams, but then I discovered that we actually start several of these
  // streams and we use a few different techniques to do so...
  test("Stream test - awakeEvery and multiple workers") {
    // Adjust this to change how many workers process the work
    val numWorkers = 2L

    // Simple little flag used to stop workStream eventually so it doesn't run forever
    val workToDoRef: Ref[IO, Int] = Ref[IO].of(10).unsafeRunSync()

    // Our work function, where we put the guts of the work we want done repeatedly.
    // This does "one unit of work"
    def doWork(workToDo: Ref[IO, Int]): IO[Boolean] =
      workToDo.modify { workUnitsToDo =>
      if (workUnitsToDo > 0) {
        println(s"doWork at ${java.time.Instant.now} (work left: ${workUnitsToDo})")
        (workUnitsToDo - 1, true)
      } else {
        (0, false)
      }
    }

    // Builds a Stream that does one unit of work and then if there is more work to do, becomes
    // a Stream that will do one unit of work.
    def workStream: Stream[IO, Unit] =
      Stream.eval((doWork(workToDoRef)).attempt)
        .flatMap{
          case Left(err) =>
            Stream.eval(Sync[IO].delay(println("ERROR DOING WORK!"))) >> Stream.empty
          case Right(moreToDo) =>
            if (moreToDo) {
              println("more work to do")
              workStream
            } else {
              println("out of work to do")
              Stream.empty
            }
        }

    // The take() call in here is not based on the original code that we're modelling, but will
    // ensure that this stream doesn't run forever
    val fullStream =
      (workStream ++ Stream.awakeEvery[IO](2.seconds).take(3).flatMap{ _ =>
        println("working the stream after awaking")
        workStream
      })

    fullStream.compile.drain.unsafeRunSync()
  }

  test("Explore the differences between Stream ++ Stream and Stream >> Stream") {
    pending

    def doWork: IO[Unit] = ???

    def workStream: Stream[IO, Unit] =
      Stream.eval(doWork.attempt)
        .flatMap{
          case Left(err) =>
            Stream.eval(Sync[IO].delay(println(s"Error while doing work: ${err}")))
          case Right(_) => Stream.empty
        }

    // In Palermo, when I used >>, it seemed to only run workStream once and then never again. Why
    // does ++ result in the stream working indefinitely?
    workStream ++ Stream.awakeEvery[IO](10.seconds).flatMap(_ => workStream)
    workStream >> Stream.awakeEvery[IO](10.seconds).flatMap(_ => workStream)
  }

}
