package org.mlamarre.sandbox.common

import cats.data._
import cats.data.Validated._
import cats.implicits._
// import cats.effect.unsafe.implicits.global
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

/**
 * Experiments using cats' Validated class (and related structures)
 */
class WorkingWithValidated extends AnyFunSuite with Matchers {

  test("Accumulating validation errors") {
    // Input to validate. According to our Planet enum, Pluto is not a planet, and of course the
    // sun is not either. So passing those values to Planet.fromString() should result in a Left.
    val potentialPlanets = List("earth", "mars", "pluto", "sol")

    // We can simply call traverse() on our inputs, but the first item to fail will stop the
    // iteration, so we'll only get one error.
    val failFastResult: Either[String, List[Planet]] = potentialPlanets.traverse(Planet.fromString)
    failFastResult.isLeft shouldBe true
    failFastResult.swap.foreach(s => s.matches("Unrecognized planet: pluto.*"))

    // If we simply convert the Either returned by Planet.fromString() to a ValidatedNel, the
    // errors will be accumulated on the "left" and the traverse() does not "early out". (Note that
    // Validated's equivalents to Either's Left/Right are Invalid/Valid, respectively.)
    val allFailsResult: ValidatedNel[String, List[Planet]] =
      potentialPlanets.traverse(s => Planet.fromString(s).toValidatedNel)
    allFailsResult.isInvalid shouldBe true
    allFailsResult.swap.foreach(_.size shouldBe 2)
  }

  test("Accumulating different chunks of validation errors") {
    // Suppose we do a validation step that looks at one thing, and results in a ValidatedNel.
    // Recall that ValidatedNel[A, B] is a shorthand for Validated[NonEmptyList[A], B], so this
    // type is the same as that of otherErr, below.
    val errs1: Validated[NonEmptyList[String], Int] =
      NonEmptyList.of("error1", "error2").invalid[Int]

    // But we want to do a separate validation step that will produce a separate ValidatedNel.
    val otherErr: ValidatedNel[String, Int] = "otherErr".invalidNel[Int]

    // We can combine the two ValidatedNels with the |+| operator.
    val allErrs = errs1 |+| otherErr

    allErrs.isInvalid shouldBe true
    allErrs.swap.foreach(_.size shouldBe 3)

    // Let us now add a Validated that is valid to the mix
    val validPlanet = Planet.fromString("earth").toValidatedNel

    // Unfortunately, Validated is not a Monad, so it does not have flatMap, so we can't read all
    // of these Validated objects in a for comprehension. But here is a way to combine them all and
    // (if they are all valid), spit out a final result.
    val result: ValidatedNel[String, Planet] = (validPlanet, errs1, otherErr).mapN((p, i1, i2) => p)

    // Now in this case, we know we're using Validated values that are Invalid, so we end up with
    // an Invalid at the end, but we can see above how we can gather a bunch of different types on
    // the "right" side to produce a single output value.
    result.isInvalid shouldBe true
    result.swap.foreach(_.size shouldBe 3)

  }
}
