package org.mlamarre.sandbox.common

// When using standard Java classes that share a name w/ a Scala class, it is common to rename the
// Java class in the imports with a "J" prepended to the class name.
import java.util.{ List => JList, ArrayList => JArrayList }

import scala.collection.JavaConverters._

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

import cats.implicits._
/**
 * Some common tips/tricks when working with Java code.
 * @author mlamarre
 * @since JDK 1.6
 */
class WorkingWithJava extends AnyFunSuite with Matchers {

  test("Java collections are generally mutable, so convert them to Scala collections ASAP") {
    val l: JList[Int] = new JArrayList[Int]()
    l should have size (0)
    l.add(3)
    l.add(8)
    l should have size (2)

    // It is generally preferable to convert Java collections to Scala collections as soon as you
    // get them back from Java code. The JavaConverters import above adds several "asScala" methods
    // for converting from Java collections to their corresponding Scala collections.
    val immutableVals: Iterable[Object] = System.getProperties.values.asScala
    immutableVals.size shouldBe System.getProperties.values.size()
  }

  test("Turn nulls into Options as soon as possible") {
    // When calling a Java method that may return you a null, wrap that in a call to Option.apply(),
    //  which will give you a Some if the function returned a non-null value, or a None if it
    // returned null.
    val goodValue = Option(System.getProperty("os.name"))
    goodValue.isDefined shouldBe true

    val badValue = Option(System.getProperty("bogus.property"))
    badValue shouldBe None
  }

  test("Be sure to handle Java's stupid exceptions") {
    // Java code loves to throw exceptions. You will often want to do something like wrap dangerous
    // calls to Java methods in something like catchNonFatal() from the Cats library to handle
    // errors more idiomatically.
    val tempFile = Either.catchNonFatal{
      java.io.File.createTempFile("prefix", "tempfile")
    }
    tempFile.isRight shouldBe true

    val oops = Either.catchNonFatal{
      // Throws IllegalArgumentException because the prefix must have length >= 3
      java.io.File.createTempFile("a", "tempfile")
    }
    oops.isLeft shouldBe true
  }

}