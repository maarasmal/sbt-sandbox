package org.mlamarre.sandbox.common

// TODO: Convert this into a test that is executable

/**
 * Matt posted this helpful walkthrough of issues composing IO and Either and exceptions. Seemed
 * worth saving and packaging up for reminder/example
 *
 * Relevant reading on the topic (linked by Mike P.):
 *  - https://www.artima.com/articles/the-trouble-with-checked-exceptions
 *  - https://alexn.org/blog/2018/05/06/bifunctor-io.html
 *
 * And these type-classes that aim to give you the best of both worlds (also linked by Mike P.):
 *  - https://typelevel.org/cats-mtl/mtl-classes/handle.html
 *  - https://typelevel.org/cats-mtl/mtl-classes/raise.html
 */
object SampleCode {

  import cats.data.EitherT
  import cats.effect.IO

  val ioe1: IO[Either[Exception, Int]] = IO.pure(Right(1))
  val ioe2: IO[Either[Exception, Int]] = IO.pure(Right(2))
  val ioe3: IO[Either[Exception, Int]] = IO.pure(Left(new IllegalArgumentException("Can't compute")))

  // for {
  //     res1 <- ioe1
  //     res2 <- ioe1
  //     res3 <- ioe1
  // } yield (res1 + res2 + res3) // This doesn't work because we're only one stack down and have Eithers

  // You could nest these for loops, but that's a lot of work.
  val result: IO[Either[Exception, Int]] = for {
    eres1 <- ioe1
    eres2 <- ioe2
    eres3 <- ioe3
  } yield {
    for {
      res1 <- eres1
      res2 <- eres2
      res3 <- eres3
    } yield (res1 + res2 + res3)
  }

  // You could use this scary-sounding thing called EitherT
  val result2: IO[Either[Exception, Int]] = (for {
    res1 <- EitherT(ioe1)
    res2 <- EitherT(ioe2)
    res3 <- EitherT(ioe3)
  } yield (res1 + res2 + res3)).value // This .value is from EitherT

  // Or you could just push the exception up into the IO
  val result3: IO[Int] = for {
    res1 <- ioe1.flatMap(IO.fromEither)
    res2 <- ioe2.flatMap(IO.fromEither)
    res3 <- ioe3.flatMap(IO.fromEither)
  } yield (res1 + res2 + res3)

  // And then if you want to pull the error back out
  val result31: IO[Either[Throwable, Int]] = result3.attempt

  // Unless you actually are going to act on the exception, most folks keep it hidden in the IO until they need to deal with it.

}

