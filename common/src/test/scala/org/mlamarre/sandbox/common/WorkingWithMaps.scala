package org.mlamarre.sandbox.common

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

import scala.collection.MapView

class WorkingWithMaps extends AnyFunSuite with Matchers {

  test("Mapping the contents of Maps") {
    val originalMap: Map[String, Int] = Map(("alpha", 1), ("beta", 2), ("gamma", 3), ("delta", 4))

    // I ran into a couple cases where I wanted to transform a Map[A,B] into a Map[A,C] via a
    // single call to map(), and I kept forgetting the proper way to do so. It seems like it should
    // be possible with something like:
    // myMap.map( (k,v) => k + v )
    // However, this gives you errors saying "wrong number of parameters; expected = 1". I was able
    // to find an answer here:
    // http://stackoverflow.com/questions/8812338/scala-mismatch-while-mapping-map/8812477#8812477
    // This test case is merely to preserve what I've found so that I don't have to google it again.

    // The map() function only gives you a single value, the key and value as a tuple. If you want
    // to unapply them to get access to both without having to use the ugly x._1, x._2 notation,
    // you need to use a partial function.
    // Thus, to transform a Map[A,B] into a Map[A,C] via the map() function, do the following:
    val newMap: Map[String, String] = originalMap.map { case (k, v) => (k, k + v) }

    // If you don't want to use a partial function, you have to use the tuple as a single value,
    // like so:
    val otherMap: Map[String, String] = originalMap.map(tup => (tup._1, tup._1 + tup._2))

    // Also note that, curly braces or not, if you only specify a single value after the =>, you
    // will get back a LIST, not a MAP! (well, an Iterable, but let's not split hairs).
    val notAMap: Iterable[String] = originalMap.map { case (k, v) => k + v }

    // If you only want to map the values, and you don't need the keys for your transformation,
    // there is a mapValues() method that is simpler to use. Note that mapValues() returns a
    // MapView, and is deprecated as of 2.13.0. See here for details:
    // https://www.scala-lang.org/api/2.13.3/scala/collection/Map.html#mapValues[W](f:V=%3EW):scala.collection.MapView[K,W]
    // In the meantime, use .view.mapValues(f).toMap
    val xformedValues: MapView[String, Int] = originalMap.mapValues(v => v + 1)
    xformedValues.get("alpha") shouldBe Some(2)

    // Until something like mapValuesStrict() is provided in a future version of Scala, this is
    // the recommended way to use mapValues(), so that the MapView is more obvious.
    val moreExplicit: Map[String, Int] = originalMap.view.mapValues(_ + 1).toMap
    moreExplicit.get("beta") shouldBe Some(3)
  }
}