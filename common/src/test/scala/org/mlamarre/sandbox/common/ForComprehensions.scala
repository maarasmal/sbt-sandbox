package org.mlamarre.sandbox.common

import cats.implicits._
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

/**
 * Various tests demonstrating the capabilities of Scala's for-comprehensions. A for-comprehension
 * is syntactic sugar for nested calls to flatMap and map.
 */
class ForComprehensions extends AnyFunSuite with Matchers {

  /**
   * Demonstrates the fundamentals of working with for-comprehensions. This method is based on a
   * long discussion that I had with Jordan that helped me finally\ wrap my head around how
   * for-comprehensions work.
   */
  test("Fundamentals of for-comprehensions") {
    // Just some sample data for demonstration purposes.
    val l1 = List(1, 2)
    val l2 = List("#", "@")
    val l3 = List(3, 4)

    // Here's a simple for-comprehension:
    val a1 = for {
      i1 <- l1
      s2 <- l2
      i3 <- l3
    } yield (i1 + i3) + s2
    a1 shouldBe (List("4#", "5#", "4@", "5@", "5#", "6#", "5@", "6@"))

    // One key thing to remember about for-comprehensions (FC's) is that the last generator
    // always translates to a call to map(). If there is more than one generator, all but the
    // last one translate to flatMap().
    //
    // The map() method merely transforms the items in a container. Thus:
    // List(A,A,A)  can be mapped to  List(B,B,B)
    //
    // The flatMap() method can both transform the items in the container, as well as the
    // NUMBER of items in the container. Thus, the following is possible:
    // List(A,A,A)  can be flat mapped to  List(B)
    // OR
    // List(A,A,A)  can be flat mapped to  List(B,B,B,B,B)
    //
    // Maps are a one-to-one mapping. For instance, given the function 2x = y, each value
    // of x can result in only one y value. So if x = 4, y can ONLY be 8.
    //
    // Flat maps can be a one-to-many mapping. Given sqrt(x) = y, if x = 4, y could be 2 OR -2
    //
    // flatMap() always returns the same type of container upon which it was invoked. So
    // l1.flatMap(...) must return a List. It could not return a Map, for instance.

    // The for-comprehension above would "de-sugar" to the following calls to flatMap and map:
    val a2 = l1.flatMap { i1: Int =>
      l2.flatMap { s2: String =>
        l3.map { i3: Int =>
          (i1 + i3) + s2
        }
      }
    }
    a2 shouldBe (List("4#", "5#", "4@", "5@", "5#", "6#", "5@", "6@"))

    // Since flatMap must return the container type upon which it was invoked (List, in this case),
    // we can see that the container returned by a FC is of the same type as the value on the right
    // of the first generator in the FC (l1, in this case). We can see that the CONTENTS may change,
    // but the container type may not. Additionally, given the way the map/flatMap calls are nested,
    // we see that each generator must operate on the same type of container. If l2 were actually a
    // map m2, we would have to change the second generator in our FC to say "s2 <- m2.toList".
    //
    // If you "slow down" the iteration and walk through it step-by-step, what you end up with is
    // the l3.map() call returning a List[String].  That is, for i1=1, s2="#", l3.map() returns
    // List(4#, 5#). We then continue iterating through the items in l2 in the enclosing flatMap
    // call.
    //
    // Since l3.map() returns List[String], the mapping function passed to l2.flatMap is really
    // mapping String => List[String]. So at "the end" of l2.flatMap(), what you really have is a
    // List that contains one List[String] per item in l3. Given the example above, when
    // when i1 = 1, just before flatMap() resolves, it essentially looks like:
    // List( List(4#, 5#), List(4@, 5@) ). This value is then "flattened" so the value actually
    // returned by l2.flatMap is List( 4#, 5#, 4@, 5@ ).
    //
    // Similarly, at "the end" of l1.flatMap(), the value that has been constructed (in this
    // example) is a List that contains two 4-element List[String]. That is then flattened into
    // a List[String] with 8 elements.

    // What if we introduce a guard condition? Those map to calls to filter().
    val a3 = for {
      i1 <- l1 if (i1 > 1)
      s2 <- l2
      i3 <- l3
    } yield (i1 + i3) + s2
    a3 shouldBe (List("5#", "6#", "5@", "6@"))

    // If you translate this FC to its flatMap/map form, it should be obvious that we want to
    // prevent any value of i1 from ever being mapped in that call to l2.flatMap() if it fails the
    // guard. Thus, the filtering must occur BEFORE the flatMap() that results from that generator,
    // which means the FC above de-sugars to look like this:
    val a4 = l1.filter { i1: Int => i1 > 1 }.flatMap { i1: Int =>
      l2.flatMap { s2: String =>
        l3.map { i3: Int =>
          (i1 + i3) + s2
        }
      }
    }
    a4 shouldBe (List("5#", "6#", "5@", "6@"))

    // The one other thing you can do inside of a FC is create temporary val objects to hold
    // intermediate computations.  This is a contrived example that doesn't change the final
    // result, but here is our original FC with a local val (temp1) created in it:
    val a5 = for {
      i1 <- l1
      temp1 = i1 * 2 // Note that you do not (and cannot) use the 'val' keyword here
      s2 <- l2
      i3 <- l3
    } yield (i1 + i3) + s2
    a5 shouldBe (List("4#", "5#", "4@", "5@", "5#", "6#", "5@", "6@"))

    // If we translate this FC to its map/flatMap form, where would temp1 be? With the guard
    // condition, it was clear that the guard had to be applied prior to the flatMap call.
    // In this case though, we want to make use of the generated value i1 when computing temp1,
    // and the val that we calculate may be referred to in the nested map/flatMap calls. Thus,
    // the only logical place for the val assignment to go is shown here:
    val a6 = l1.flatMap { i1: Int =>
      val temp1 = i1 * 2 // In de-sugared form, you DO need the 'val' keyword
      l2.flatMap { s2: String =>
        l3.map { i3: Int =>
          (i1 + i3) + s2
        }
      }
    }
    a6 shouldBe (List("4#", "5#", "4@", "5@", "5#", "6#", "5@", "6@"))

    // So what about the way we often use Eithers in our code? It is not uncommon to see functions
    // that are simply implemented with a single FC with a number of generators that just call
    // functions that all return Either, like so:
    //
    // def foo[A,B](a: A): Either[String, B] = for {
    //   x <- doStep1(a)
    //   y <- doStep2(x)
    //   b <- doStep3(y)
    // } yield b
    //
    // How do Eithers fit in to this picture? Well first, lets think about how map and flatMap
    // work on Lists. A list can be either empty or non-empty:
    //
    //     List[T]
    //     /     \
    // List()    List(t*)
    //
    // If you call map() or flatMap() on an empty list, there are no items to iterate on. But
    // those functions each return a List, so they must return an empty list (Nil, or List()).
    // If you call map() or flatMap() on a non-empty list, we have already observed that for
    // each item in the list, the mapping function is applied to the item.
    //
    // Similar to Lists, there are only two kinds of Option: None or Some. To show this in
    // a manner similar to how we diagramed List above:
    //
    //    Option[T]
    //    /      \
    // None     Some(t)
    //
    // This congruence between the two "images" for List and Option is a handy reminder then
    // of how map and flatMap work on the two structures. An Option is a container just like
    // a List, except it has a maximum capacity of 1. Thus, when map or flatMap is called on
    // an Option that happens to be a None, there is no "contained item" to which the mapping
    // function can be applied, but we must return the same type of container we started with,
    // (i.e., an Option) so we return an Option that is the value None. If we call map/flatMap on
    // an Option that is a Some, we apply the mapping function to each item in the container (in
    // this case, just one item) and return a new Option that contains the mapped value.
    //
    // An Either is just another type of single-item container, very similar to Option.
    // They have a similar relationship diagram to List and Option as well:
    //
    //       Either[E, A]
    //       /    \
    //  Left(E)    Right(A)
    //
    // In the case of Either, map and flatMap simply return a Left if you call it on a Left. As with
    // the empty list and None, the mapping function is not applied, even though in the case where
    // you have a Left, there *is* an item in the container (but its type likely doesn't match the
    // mapping function's input type, so you couldn't apply it to the value in the Left anyway) If
    // the Either is actually a Right, then it calls the mapping function on the item stored in the
    // Right. As with List and Option, map and flatMap on Either objects must return the same type
    // of container on which they were invoked. In this case, that is another Either. But you can
    // change the types of the items inside the disjunction.

    val dj1 = Right[Throwable, Int](1)
    val dj2 = Right[Throwable, String]("hi")
    val dj3: Either[Throwable, String] = for {
      i1 <- dj1
      s2 <- dj2
    } yield s2 + " " + i1

    // Even though dj1 is Either[Throwable, Int], the result will have a String on the right
    // because the value returned by the final call to map() (i.e., s2 + " " + i1) is of type
    // String. The *container* type is forced by the type of the first generator in the FC, but not
    // the type of the contents within that container.
    dj3.isRight shouldBe true
    dj3.foreach { rightSide => rightSide shouldBe ("hi 1") }

  }

  test("Stopping an FC early with a guard") {
    val oneToThree = (1 to 3).toList
    val century = (100 to 102).toList

    // Stop the "second loop" part of the way through
    val res1 = for {
      x <- oneToThree
      y <- century if (y <= 101)
    } yield x + y
    res1 should contain theSameElementsAs (List(101, 102, 103, 102, 103, 104))

    // Skip the entire "second loop". If a guard breaks an iteration, the "yield" part of the
    // statement does not execute for that iteration. In this case, every iteration of the FC is
    // aborted by the guard, so the yield never happens even once, resulting in an empty list.
    // Note though that the type of the overall FC expression is still determined by the type of
    // the expression following the "yield". In this case, that means Int.
    val res2 = for {
      x <- oneToThree
      y <- century if (false)
    } yield x + y
    res2.isEmpty shouldBe true
  }

  // TODO: This test was written back when we were using Scalaz's disjunction type, \/, which is
  // roughly equivalent to modern Either. Implicits enabled the calls like 1.right[String], and
  // allowed the use of the guard to compile. As mentioned in the note below, this worked here, but
  // would typically explode at runtime for most real-world applications.
  // KEY TAKEAWAY OF THIS EXAMPLE: Don't try to use guards w/ Either or \/
  test("Stopping a disjunction-driven FC early with a guard") {
    pending
    // NOTE: This only works because I put a String on the left without thinking about it! See the
    // note in UsingDisjunctions.scala about using guards with disjunctions in FC's for a detailed
    // explanation as to why you never want to do this in practice.
    // val foo = for {
    //   r1 <- 1.right[String]
    //   r2 <- 2.right[String] if (r2 > 3)
    //   r3 <- 3.right[String]
    // } yield r1 + r2 + r3

    // foo.isLeft shouldBe true
  }

  test("Generating tuples on the right side of an Either") {
    // Attempting to extract a tuple from a Right in a FC generator will de-sugar into a call to
    // withFilter/filter. In this case, filter() requires that you have an implicit Monoid for the
    // type on the left side of your Either.

    // So this works, because there *is* an implicit Monoid[String] in Scalaz.
    // UPDATE 2020-07-15: It worked with Scalaz. It does NOT work with Either and cats.
    // val foo = for {
    //   (a, b) <- (1, 3).right[String]
    //   c <- 5.right[String]
    // } yield a + c
    // foo.isRight shouldBe true
    // foo foreach (_ shouldBe (6))

    // The above FC de-sugars to roughly:
    // (1, 3).right[String]
    //   .withFilter { x => /* check that x matches the extractor */ }
    //   .flatMap { x => val (a, b) = x; 5.right[String].... }
    // Our tuple will never fail the predicate passed to withFilter, but if you look at the
    // implementation of \/.filter(), you will see that it still requires a Monoid for your left
    // side type.

    // In reality, you are probably going to have something like Exception or Err on the left side
    // of your Either, for which there is NOT a Monoid defined. So if the above FC used Exception
    // in place of String, it would not compile. But you can accomplish the same thing with one
    // extra line to do your tuple extraction, and avoid a call to filter() on the Either.
    val bar = for {
      t <- Right[Exception, Tuple2[Int, Int]](1, 3)
      (a, b) = t
      c <- Right[String, Int](5)
    } yield a + c
    bar.isRight shouldBe true
    bar foreach (_ shouldBe (6))
  }

  test("Extracting collection values in a for comprehension") {
    // Raja asked about how to basically have a line like so in a FC:
    //   Array(k,v) <- str.split("=").toVector
    // There are some error cases that mean the right side of that generator should probably be
    // pushed down into a function, but in general, I found the syntax for unapplying collections
    // looks like this:

    val extractedPairs = for {
      // Iterate over each element in the Vector
      x <- Vector("a=b", "c=d", "m=l")
      // Split the value on "=", which returns an Array, which we convert to Vector so that we can
      // use its unapply() to extract the values. In this case, tail should be empty each time.
      Vector(k, v, tail @ _*) = x.split("=").toVector
    } yield (k, v)

    extractedPairs shouldBe (Vector(("a", "b"), ("c", "d"), ("m", "l")))
  }

  test("Oh yeah, for-comprehensions can act like Java's 'for' loops too, I forgot") {
    // I have been writing Scala code since approximately 2015. In all that time, I cannot remember
    // when (if ever) I have needed to write my own loop. Functions like map, flatMap, fold, find,
    // exists, etc. should handle nearly any situation that you encounter.
    var accum = 0
    for (i <- 1 to 3) accum += i
    accum shouldBe (6)

    // A more Java-looking loop with multiple statements in the loop body.
    accum = 0
    for (i <- 1 to 5) {
      println(s"Processing ${i}")
      val foo = i * 2
      accum += (foo / 2)
    }
    accum shouldBe (15)

    // Still though, this is way cooler, isn't it?
    val acc = (1 to 5).foldLeft(0)((x, next) => x + next)
    acc shouldBe (15)
  }

}

