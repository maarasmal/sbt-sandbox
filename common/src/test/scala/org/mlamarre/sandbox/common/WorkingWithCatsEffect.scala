package org.mlamarre
package sandbox
package common

import cats.implicits._
import cats.effect._
import cats.effect.unsafe.implicits.global
import fs2._
import org.scalatest.BeforeAndAfterAll
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._
import scala.concurrent.duration._

class WorkingWithCatsEffect extends AnyFunSuite with Matchers with BeforeAndAfterAll {

  test("Combining two IO's to run in parallel") {
    val io1 = IO{
      println(s"running IO 1")
      42
    }

    val io2 =  IO{
      println(s"running IO 2")
      99
    }

    val result = (io1, io2).parMapN{ case (a, b) => a + b }.unsafeRunSync()
    result shouldBe 141
  }

  test("Chaining flatTap calls") {
    val computeValues: IO[(Int, Map[Int, String])] = IO{
      val m = Map(1 -> "abc", 2 -> "def")
      val x = 27
      (x, m)
    }

    // Calling flatTap lets you "sample" the values from the IO without having to map them to
    // some other value (or ending your your map() with `identity`). For example, you compute a
    // value in an IO and you want to return that IO, but before you do, you want to dispatch
    // some sort of event or HTTP call or something based on the computed value. Multiple calls
    // to flatTap leave the original computed value intact, so while you can do many things inside
    // of one flatTap call, it might be cleaner to break them up into individual chunks for
    // better modularization and readability.
    computeValues.flatTap{
      case (i, lookup) =>
        IO{
          println("first tap")
          i shouldBe 27
          lookup(1) shouldBe "abc"
        }
    }.flatTap{
      case (i2, lookup2) =>
        IO{
          println("second tap")
          i2 shouldBe 27
          lookup2(2) shouldBe "def"
        }
    }.unsafeRunSync()
  }
}