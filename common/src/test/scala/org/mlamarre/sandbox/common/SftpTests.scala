package org.mlamarre.sandbox.common

import cats.implicits._
import cats.effect._
import cats.effect.unsafe.implicits.global
import net.schmizz.sshj.SSHClient
import net.schmizz.sshj.sftp.SFTPClient
import org.scalatest.BeforeAndAfterAll
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._
import scala.concurrent.duration._

class SftpTests extends AnyFunSuite with Matchers with BeforeAndAfterAll {

  private def createSshjClient(
    remoteHost: String,
    username: String,
    passwd: String
  ): SSHClient = {
    val client = new SSHClient()
    client.loadKnownHosts()
    client.connect(remoteHost)
    client.authPassword(username, passwd)
    client
  }

  test("basic sshj usage test") {
    pending
    val client: SSHClient = createSshjClient("localhost", "mlamar0934k", "whatever")
    val sftp: SFTPClient = client.newSFTPClient()

    sftp.get("/etc/hosts", "/tmp/sftp-test-etc-hosts")

    sftp.close()
    client.disconnect()
  }
}