package org.mlamarre.sandbox.common

import cats.implicits._

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

class RegularExpressions extends AnyFunSuite with Matchers {

  test("Regular expressions support unapply, and therefore things like match-case statements") {
    // Regex to match a 1-3 digit number, followed by "% ", then any other text, capturing the
    // groups consisting of the 1-3 digit number and the other text following the "% ".
    val myRegex = """(\d{1,3})% (.*)""".r

    // Regex supports unapply, so it can be used in match-case statements.
    // Fun fact, these variables used to be 'a' and 'b', but a variable named 'a' interferes with
    // matchers that read "foo shouldBe a [Bar]".
    val (num, str) = "50% (2.0 of 4.0MB) (0m 2s)" match {
      case myRegex(pct, rest) => (pct.toInt, rest)
      case s: String => (0, s)
    }
    num shouldBe 50
    str shouldBe "(2.0 of 4.0MB) (0m 2s)"

    // You can also use unapply as follows, but if the pattern doesn't match, you'll get a
    // MatchError.
    val myRegex(x, y) = "75% (3.0 of 4.0MB) (0m 2s)"
    x shouldBe "75" // Note, the type of 'x' is String, not Int, as was the case for 'num' above.
    y shouldBe "(3.0 of 4.0MB) (0m 2s)"

    val dj = Either.catchNonFatal {
      val myRegex(foo, bar) = "This string does not match the pattern."
      fail("We should never get here")
    }
    dj.isLeft shouldBe true
    dj.swap.foreach {
      case _: MatchError => succeed
      case e => fail(s"Expected a MatchError, but caught: $e")
    }
  }

  test("Pattern matching regular expressions without groups") {
    // Can you pattern match with a regex that doesn't contain groups?
    val pattern = """The specified value \(.*\) was not valid""".r

    sealed trait Result
    case object PatternMatch extends Result
    case object NoPatternMatch extends Result

    val result1: Result = "The specified value (foo) was not valid" match {
      // MUST include the () with "pattern" so it doesn't treat "pattern" as a variable, but rather
      // calls unapply() to try to match the regex. Without the "()", the second case is
      // unreachable, because the first case would always match.
      case pattern() => PatternMatch
      case str: String => NoPatternMatch
    }
    result1 shouldBe (PatternMatch)

    // Check a second string, just to show that using () on the first 'case' ensures the regex is
    // checked, not just a simple variable match, so we DO fall through to the second 'case' and
    // get failure.
    val result2: Result = "The specified value (foo) was TOTALLY valid" match {
      case pattern() => PatternMatch
      case str: String => NoPatternMatch
    }
    result2 shouldBe (NoPatternMatch)
  }

  test("Simple pattern matches") {
    // These just use things built into Java, which can often be simpler than using Scala's Regex.
    "12940383".matches("""\d{8}""") shouldBe true
    "0x0294abfe0a".matches("""0x[0-9a-fA-F]{10}""") shouldBe true
  }
}
