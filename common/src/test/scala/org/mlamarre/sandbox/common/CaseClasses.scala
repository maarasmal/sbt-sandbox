package org.mlamarre.sandbox.common

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

class CaseClasses extends AnyFunSuite with Matchers {

  case class Foo(x: Int) {
    // Define an alternate ctor. Perhaps it is easier to do this to support Java in some cases,
    // such as allowing them to pass a String instead of a Scala "enum" object.
    def this(s: String) { this(s.toInt) }
  }

  test("Instantiating case classes with alternate constructors") {
    // Instantiate a Foo, using the default apply() method generated for case classes.
    val a = Foo(2)

    // To create a Foo with the alternate constructor, you have to use "new".
    val b = new Foo("2")

    a should be(b)
  }
}
