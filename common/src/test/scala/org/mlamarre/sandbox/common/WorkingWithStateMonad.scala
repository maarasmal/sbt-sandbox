package org.mlamarre.sandbox.common

import cats.data.State
import cats.implicits._
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

/**
 * This class demonstrates some usages of the State monad from cats.
 */
class WorkingWithStateMonad extends AnyFunSuite with Matchers {

  // Many examples of State will show a for comprehension making calls to several things that
  // return State. But sometimes you just need to update a single thing in one place. This turns
  // out to play really nicely with other cats enrichments like traverse.
  test("Simple single-statement use of the State monad") {

    // We need to add new values to this cache. Note that the cache is a val.
    // Yes, we don't need State to do this particular update since Map and List play nice and the
    // data types are trivial and compatible. This is just for demonstration purposes. Imagine that
    // the thing you need to update is some custom class of yours instead of a Map.
    val initialCache: Map[String, Int] = Map("a" -> 12, "b" -> 99, "c" -> 82)
    val newValues = List(("m", 101), ("g", 102))

    // Iterate over the new values and for each one, construct a state transition that will update
    // the state (the cache) and produce a value that is the set of keys that were added or updated
    // in the cache.
    val doCacheUpdate: State[Map[String, Int], List[String]] =
      newValues.traverse{ case (ns, ni) =>
        State[Map[String, Int], String] { cache =>
          val updated: Map[String, Int] = cache.concat(List((ns, ni)))
          (updated, ns)
        }
      }

    // So now we have a state transition that will run when we apply an initial value:
    val (updatedCache, updatedKeys) = doCacheUpdate.run(initialCache).value

    updatedKeys should contain theSameElementsAs newValues.map(_._1)
    updatedCache shouldBe initialCache.concat(newValues)
  }
}