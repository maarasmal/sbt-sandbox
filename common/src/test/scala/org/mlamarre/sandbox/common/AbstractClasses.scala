package org.mlamarre.sandbox.common

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

import java.util.{ Properties => JProperties }

class AbstractClasses extends AnyFunSuite with Matchers {

  test("Order of operations when instantiating classes derived from abstract base classes") {
    // This test came about just to verify an error that I was seeing in production code. The
    // base class was calling a method that was being overridden by the child class. But
    // the child class implemented the method by referring to one of its member fields that,
    // because the call was happening during the "constructor", was not yet initialized (parent
    // class fields are initialized first). I left the println() calls in there so you can verify
    // the order of execution if you examine the test output.

    abstract class Foo {
      def methodForInit: String = {
        println("In Foo.methodForInit")
        "foo"
      }
      // Call a method to set the value of z
      val z = methodForInit
      val a: Int
      println("I'm running in Foo")
      val b: String
      def fooMethod = (a, b)
    }

    class Bar(c: String) extends Foo {
      val props = new JProperties
      props.setProperty("mykey", "myval")
      // Even though this override occurs in the code after props is declared, when the
      // base class calls it, props will not have been initialized yet.
      override def methodForInit: String = {
        println("In Bar.methodForInit")
        if (props == null) println("props is null, noob")

        // This will always NPE.
        props.getProperty("mykey")
      }
      override val a = 2
      println("I'm running in Bar")
      override val b = "bc"
      def barMethod = (a, b, c)
      override def toString = "(" + a + ", " + b + ", " + c + ")"
    }

    try {
      val x = new Bar("ab")
      fail("Should have gotten an exception, but got x.z = " + x.z)
    } catch {
      case e: NullPointerException => succeed
      case other: Throwable => fail(s"Expected exception was not thrown ${other.getMessage()}")
    }
  }

}