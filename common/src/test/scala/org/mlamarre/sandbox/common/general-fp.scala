package org.mlamarre
package sandbox
package common

import org.scalatest.BeforeAndAfterAll
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

class GeneralFpTests extends AnyFunSuite with Matchers with BeforeAndAfterAll {

  test("Closures can encapsulate and hide mutable state") {
    println("Hello there!")

    // This exercise is derived from a belated realization I had while watching, of all things,
    // a course on Javascript closures. I have used and leveraged this technique before, but for
    // whatever reason, it never fully "clicked" until watching that video. I immediately tried
    // writing a similar example in Ammonite, which is now captured below for future reference.

    // Sometimes we just need mutable data. But we want to hide it as best as possible to avoid
    // polluting the codebase with vars and globals. Things like Akka actors can provide safe
    // access to mutable state, but that is a heavyweight solution that is often not appropriate,
    // especially if your application doesn't use Akka already. Note that while the solution below
    // will safely handle mutable data, it may not be the ideal way to handle multiple threads
    // working with state data simultaneously. That is outside the scope of this discussion.

    // To avoid globals, mutable collections, Akka, etc., we can instead hide the mutable state
    // inside of a closure. In the below function, inner() closes over counter, so even though
    // outer() terminates, the underlying counter variable is not garbage collected, since it
    // is still referenced by the function object returned by outer().
    def outer: () => Int = {
      var counter = 0;
      def inner(): Int = {
        counter += 1
        counter
      }
      inner
    }

    // Running outer() generates a function object, but does not increment the instance of counter
    // that it encapsulates.
    val foo: () => Int = outer
    foo() shouldBe (1)
    foo() shouldBe (2)

    // A second call to outer() generates a separate function object, so calling that resulting
    // function does not affect the state encapsulated earlier in foo.
    val bar = outer
    bar() shouldBe (1)
    foo() shouldBe (3)

    def listOuter[A]: A => List[A] = {
      var l = List.empty[A]
      def inner(a: A): List[A] = {
        l = l.prepended(a)
        l
      }
      inner
    }

    val prepend: String => List[String] = listOuter[String]
    prepend("foo") shouldBe (List("foo"))
    prepend("bar") shouldBe (List("bar", "foo"))
  }
}
