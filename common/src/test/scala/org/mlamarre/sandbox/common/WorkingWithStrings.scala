package org.mlamarre.sandbox.common

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

/**
 * Simple string manipulation junk
 */
class WorkingWithStrings extends AnyFunSuite with Matchers {

  test("Convert a single byte to a hex string") {
    // If you have scodec-bits available in your project, that provides a lot of handy utilities
    // for doing things with bits and bytes. If you don't, there are some standard Scala functions,
    // but they may not do quite what you want.

    // You can call toHexString on your lone byte. EZPZ, right? This is an enrichment provided by
    // Scala.
    val b1: Byte = 0x01.toByte
    b1.toInt.toHexString shouldBe "1"
    // No leading zero, but that's not a big deal

    // So suppose you're reading a hex string and converting it to a Byte for use in your code, but
    // then you want to print an error message with the value. For the error string, you want to
    // convert it back to hex.
    val b2: Byte = 0xAA.toByte
    b2.toInt.toHexString shouldBe "ffffffaa"
    // Wait, wtf?

    // That's sign extension. If the first bit of your byte is a 1, you're going to get that sort
    // of conversion, since b2 converted to an Int (and to a RichInt) in order to call toHexString.
    // So let's use good old format strings.

    "%02X".format(b1) shouldBe "01"
    "%02X".format(b2) shouldBe "AA"
    "%02X".format(0xFF.toByte) shouldBe "FF"
    // Much better

  }
}