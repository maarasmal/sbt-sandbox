package org.mlamarre.sandbox.common

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

import scala.annotation.tailrec

class CollectionTricks extends AnyFunSuite with Matchers {

  case class Foo(id: String, data: Int)

  test("Transforming a List[T] into a Map[T.id, T]") {
    val fooList = List(Foo("mike", 1), Foo("esmond", 2), Foo("alan", 3))

    // Duh, so easy. First, just transform the list from a List[T] into a List[(K, T)], where K is
    // the type of the key
    val tuples = fooList.map(item => (item.id, item))

    // Then, just call toMap on the list of tuples. This will turn the first item in each tuple
    // into the key, and the second value into the value.
    val fooMap = tuples.toMap

    fooMap("mike") should be(Foo("mike", 1))

    // Unfortunately, as pointed out in this thread: http://stackoverflow.com/a/3249462
    // The performance of the above pattern can be pretty bad for large data sets, because you end
    // up iterating over the entire collection twice, and the way the map gets constructed is
    // apparently pretty inefficient too.

    // This solution was also suggested, and might be faster, although I haven't tested it.
    val foldedMap = fooList.foldLeft(Map[String, Foo]()) { (m, f) => m + (f.id -> f) }
    foldedMap("alan") should be(Foo("alan", 3))

  }

  test("Folding stuff") {
    val one = List(1)

    val answer = one.fold(0)(_ + _)

    answer should be(1)

    List(1, 2).fold(0)(_ + _) should be(3)
  }

  // Options are collections too, you know...
  test("Folding options") {
    def getStr(mkNull: Boolean): String = if (mkNull) null else "Harry"
    def trace(str: String) = println(str)

    Option(getStr(false)).fold("Hermione")(x => x) should be("Harry")
    Option(getStr(true)).fold("Hermione")(x => x) should be("Hermione")

    // First, I wanted to make a call to getOrElse.
    (Option(getStr(true)) getOrElse "Harry") should be("Harry")

    // But in my code, I wanted to add a trace() call to log which branch I took.
    // This was easy to add in the "else" clause
    Option(getStr(true)) getOrElse {
      trace("Took the 'orElse' branch!")
      "Harry"
    }

    // But where could I add a trace() call for the Some case? That's when it occurred
    // to me that 'fold' also lets me reduce an Option down to the value it contains but
    // allows me to specify two blocks, giving me a convenient place to add trace() calls
    // for both the None and Some cases.
    Option(getStr(true)).fold {
      trace("Executing the 'None' case to return 'Harry'")
      "Harry"
    } { someVal =>
      trace("Executing the 'Some' case")
      someVal
    } should be("Harry")

    // Note that while folding over, say, a collection of Int, the "zero" case is strict and
    // will always be used.
    List(1, 2, 3).fold {
      trace("Not lazily evaluated, so this always happens!")
      0
    }(_ + _)
    Option(getStr(false)).fold {
      trace("This argument to 'fold' is lazily evaluated, and in this case should never be printed!")
      "Ron"
    } { someVal =>
      trace("Executing the 'Some' case")
      someVal
    } should be("Harry")

  }

  test("Pattern matching on type rename") {
    // In secure-cast, I ran into an issue when trying to pattern match against a type that was a
    // rename of a Seq, but at runtime, was actually a Vector. For some reason, Vector doesn't
    // support pattern matching in the same way as Seq.
    type MyInts = Seq[Int]

    val foo: MyInts = Seq(1, 2, 3)
    val bar: MyInts = Vector(1, 2, 3)

    var hitCatchallCase = false

    @tailrec def loopR(values: MyInts, newValues: List[Int]): List[Int] = {
      values match {
        case Seq() => newValues
        case item :: theRest => loopR(theRest, item :: newValues)
        // Even though 'values' is of type MyInts, and should therefore be a Seq, if you pass in a
        // Vector there, it will not match the above two cases.
        case other =>
          hitCatchallCase = true
          newValues
      }
    }

    val newFoo = loopR(foo, List())
    hitCatchallCase should be(false)
    newFoo should contain theSameElementsInOrderAs (foo.reverse)

    val newBar = loopR(bar, List())
    hitCatchallCase should be(true)
    newBar should be('empty)

    hitCatchallCase = false
    val emptySeq = loopR(Vector(), List())
    hitCatchallCase should be(false)
    emptySeq should be('empty)

  }

  test("Converting None to Option[T]") {
    def foo(x: Int): Option[String] = {
      val oVal = if (x % 2 == 0) Option(x) else None
      // Even though oVal is an Option[Int], in the case where it's a None, the map()
      // call returns the original item, which is then automatically converted to an
      // Option[String] to comply with the return type specified for the function.
      oVal.map(item => item.toString)
    }

    foo(2) should be(Some("2"))
    foo(3) should be(None)
  }

  test("What is the difference between 'find' and 'collectFirst'?") {
    // The 'find' and 'collectFirst' methods seem to be very similar. For homogenous collections,
    // they largely are the same. Probably the biggest difference is that because 'collectFirst'
    // takes a partial function, you can pattern match on the items in the collection and unapply
    // the object to pull fields out if that is handy for the purposes of the call. For instance,
    // if you have a collection of Option[T], you can do something like:
    // myFoos.collectFirst{ case Some(f) if f.someField == "desiredValue" => f }

    case class Foo(id: Int, data1: String, data2: String)
    val foos = Seq(
      Foo(1, "mike", "lamarre"),
      Foo(2, "esmond", "lee"),
      Foo(3, "eric", "berry"),
      Foo(4, "mike", "pilquist"))

    // The following calls to 'find' and 'collectFirst' are equivalent.
    val c1 = foos.collectFirst { case f if f.data1 == "mike" => f }
    val f1: Option[Foo] = foos.find(_.data1 == "mike")
    c1.isDefined shouldBe true
    c1 should be(f1)
    c1.foreach(_.id should be(1))

    // 'collectFirst' is more powerful when the collection is heterogenous.
    case class Bar(id: Int, bdata1: String, bdata2: String)
    val foosAndBars = Seq(
      Foo(1, "mike", "lamarre"),
      Bar(1, "mike", "pilquist"),
      Foo(2, "eric", "berry"),
      Bar(2, "eric", "lamarre"))

    // Find the first Foo whose 'data1' field is "eric".
    val c2: Option[Foo] = foosAndBars.collectFirst { case f: Foo if f.data1 == "eric" => f }
    // 'find' would be uglier to use in this case, since we would have to convert items in the
    // collection to either 'Foo' or 'Bar' to be able to test them. We basically have to write
    // something that looks like the PF we passed to 'collectFirst', but we have to actually
    // specify the entire function, not just a partial.
    val f2 = foosAndBars.find {
      _ match {
        case f: Foo => f.data1 == "eric"
        case b: Bar => false
      }
    }

    // And while 'find' here returns the same item that 'collectFirst' did, the compiler isn't able
    // to infer the return type as accurately as it can with the 'collectFirst' call, requiring us
    // to use 'asInstanceOf', which sucks.
    c2.isDefined shouldBe true
    f2.isDefined shouldBe true
    f2.foreach { found =>
      found shouldBe a[Foo]
      found.asInstanceOf[Foo] should be(c2.getOrElse(Foo(99, "ERROR", "ERROR")))
    }

    // As another demonstration of the usefulness of 'collectFirst' being able to "unapply" with
    // its partial function, consider a list of optional Foos:
    val optFoos = Seq(
      None,
      Option(Foo(8, "eric", "berry")),
      None,
      Option(Foo(9, "mike", "lamarre")),
      Option(Foo(10, "mike", "smith")))

    // With 'collectFirst', we can "skip" the None values with our partial function, and easily
    // unpack the Some values to get at the inner Foo objects:
    val result: Option[Foo] = optFoos.collectFirst { case Some(f) if f.data1 == "mike" && f.id > 9 => f }
    result.isDefined shouldBe true
    result should be(optFoos.reverse.head)
  }

  test("Mapping or folding Options") {
    // Es and Raja wanted to turn an Option[A] into an Option[B]. At first, it was thought that
    // map() was not appropriate, because based on the data in A, they may still want to return a
    // None. So fold() was employed.
    def doAFold(opt: Option[Int]): Option[String] =
      opt.fold(Option.empty[String])(item => Option(item.toString))
    doAFold(Option(3)) should be(Option("3"))
    doAFold(None) should be(None)

    // While Es and I were talking, we realized that you could do map(), followed by flatten(). Or,
    // duh, flatMap(). This seems a lot cleaner.
    def doAFlatMap(opt: Option[Int]): Option[String] = opt.flatMap(item => Option(item.toString))
    doAFlatMap(Option(5)) should be(Option("5"))
    doAFlatMap(None) should be(None)
  }

  test("Lifting collections and arrays") {
    // Found this trick in the context of a larger post about Scalaz's Validation type:
    // http://blog.lunatech.com/2012/03/02/validation-scala

    // You can call .lift on a collection of A's of unknown size and get back a function
    // Int => Option[A]
    val liftedList = List(1, 3, 5).lift
    liftedList(0) should be(Some(1))
    liftedList(2) should be(Some(5))
    liftedList(3) should be(None)

    // You can do the same for arrays:
    val strings = "abc;def;ghi;xyz".split(";").lift
    strings(0) should be(Some("abc"))
    strings(9) should be(None)
  }

  test("Combining two Option values") {
    // Es asked how to combine the strings in two Options into a single Option[String]. Cats
    // provides a |+| operator for semigroups.
    val o1: Option[String] = Some("abc")
    val o2: Option[String] = Some("xyz")

    import cats.syntax.semigroup._
    val result = o1 |+| o2
    result shouldBe Some("abcxyz")

    // Note that if you want the result to look something like "abc, xyz", it is probably
    // simpler to just use standard library (non-Cats) stuff
    val commaResult = (o1.toList ++ o2.toList).mkString(", ")
    commaResult shouldBe "abc, xyz"
  }

  test("Combining two Map values") {
    // Es and I were trying to combine two `Map[A, List[B]]` values together such that the lists
    // would be merged if the same key existed in both maps.
    val m1 = Map("a" -> List(1,2,3), "b" -> List(4,5,6))
    val m2 = Map("a" -> List(3,4,5))

    // When you use ++ on two Maps, if the same key is in both Maps, the value from the latter Map
    // will replace the value from the first Map
    val result1 = m1 ++ m2
    result1 shouldBe Map("a" -> List(3, 4, 5), "b" -> List(4, 5, 6))

    // Cats again provides us with |+| for Map that does what we were looking for:
    import cats.syntax.all._
    val result2 = m1 |+| m2
    result2 shouldBe Map("a" -> List(1, 2, 3, 3, 4, 5), "b" -> List(4, 5, 6))
  }

  test("Parallel processing a set of values") {
    import cats.effect.IO
    import cats.effect.unsafe.implicits.global
    import cats.syntax.all._

    val values = List(1,2,3,4,5,6,7,8,9,0)
    val valsIo = values.parTraverse(i => IO{
      println(s"processing value: $i")
      i * 2
    })
    val valsResult = valsIo.unsafeRunSync()
    valsResult shouldBe values.map(_ * 2)
  }

  test("Parallel processing a pair of values") {
    import cats.effect.IO
    import cats.effect.unsafe.implicits.global
    import cats.syntax.all._

    // Let's try a tuple first
    val values = (2,3)
    val valsIo = values.parTraverse(i => IO{
      println(s"processing tuple value: $i")
      i * 2
    })
    val valsResult = valsIo.unsafeRunSync()
    // Wait, what?! Shouldn't the expected value be (4, 6)?
    valsResult shouldBe (2, 6)

    // It turns out that a bunch of the "standard" operations on Tuples are defined to only apply
    // the provided function to the last value of the tuple:
    Tuple2(9, 8).map(_ * 2) shouldBe (9, 16)
    Tuple3(1,2,3).map(_ * 2) shouldBe (1,2,6)

    // Cats provides the operations `map` and `parTraverse` on tuples. If you want to map over
    // both items in a tuple-2, it also provides bimap, though you have to provide two functions:
    Tuple2(4,5).bimap(_ * 2, _ * 3) shouldBe (8, 15)
    // It does not appear that there are corresponding trimap, quadmap, etc. functions for tuples
    // with an arity beyond 2.

    // As we saw in the previous test, parTraverse works on a List. Cats does provide parBitraverse,
    // which does what we want, though as with bimap(), we have to provide two function parameters:
    val valsIoAgain = Tuple2(2,7).parBitraverse(a => IO(a * 2), b => IO(b * 2))
    valsIoAgain.unsafeRunSync() shouldBe (4, 14)
  }

  test("Sorting more complex collections with custom ordering") {
    trait StatusType { def value: Int }
    case object OutOfSync extends StatusType { def value: Int = 8 }
    case object Synchronized extends StatusType { def value: Int = 9 }

    object StatusType {
      implicit val ordering: Ordering[StatusType] = Ordering.by[StatusType, Int](_.value)
    }

    // We want a custom ordering for our List[Option[StatusType, Long]] where the Long part is
    // ALWAYS sorted descending, regardless of how the Option and StatusType parts are sorted. We
    // have to fully define our own Ordering here because of the way that the standard Ordering
    // is implemented. Originally we tried to use Ordering.Option() with only the "inner" part
    // defined by us, but if you look at how Ordering implements `reverse`, you can see that our
    // overridden `reverse` was never called, so our customization was ignored in the reverse case.
    // val statusTupleOrdering: Ordering[Option[(StatusType, Long)]] =
    //   new Ordering[Option[(StatusType, Long)]] { outer =>
    //     private val ascLongOrd: Ordering[Long] = implicitly[Ordering[Long]]
    //     private val descLongOrd = ascLongOrd.reverse

    //     private def doComp(
    //         x: Option[(StatusType, Long)],
    //         y: Option[(StatusType, Long)],
    //         isReverse: Boolean
    //     ): Int = {
    //       (x, y) match {
    //         case (None, None) => 0
    //         case (None, _) => if (isReverse) 1 else -1
    //         case (_, None) => if (isReverse) -1 else 1
    //         case (Some((xs, xl)), Some((ys, yl))) =>
    //           val syncResult = if (isReverse) StatusType.ordering.compare(ys, xs) else StatusType.ordering.compare(xs, ys)
    //           // If everything else is equal, we ALWAYS compare the longs using descending sort,
    //           // regardless of isReverse
    //           if (syncResult == 0)
    //             xs match {
    //               case Synchronized => ascLongOrd.compare(xl, yl)
    //               case _ => descLongOrd.compare(xl, yl)
    //             }
    //           else syncResult
    //       }
    //     }

    //     override def compare(x: Option[(StatusType, Long)], y: Option[(StatusType, Long)]): Int =
    //       doComp(x, y, false)

    //     override val reverse: Ordering[Option[(StatusType, Long)]] =
    //       new Ordering[Option[(StatusType, Long)]] {
    //         override def compare(x: Option[(StatusType, Long)], y: Option[(StatusType, Long)]): Int = {
    //           doComp(x, y, true)
    //         }
    //         override val reverse: Ordering[Option[(StatusType, Long)]] = outer
    //       }

    //     // The scala API docs say if you override `reverse` and change the default behavior, you
    //     // MUST also override `isReverseOf`
    //     override def isReverseOf(other: Ordering[_]): Boolean = other == this.reverse
    //   }
    val statusTupleOrdering: Ordering[Option[(StatusType, Long)]] =
      new Ordering[Option[(StatusType, Long)]] { outer =>
        private val ascLongOrd: Ordering[Long] = implicitly[Ordering[Long]]
        private val descLongOrd = ascLongOrd.reverse

        private def doComp(
            x: Option[(StatusType, Long)],
            y: Option[(StatusType, Long)],
            isReverse: Boolean
        ): Int = {
          (x, y) match {
            case (None, None) => 0
            case (None, _) => -1
            case (_, None) => 1
            case (Some((xs, xl)), Some((ys, yl))) =>
              val syncResult = StatusType.ordering.compare(xs, ys)
              // If everything else is equal, we ALWAYS compare the longs using descending sort,
              // regardless of isReverse
              if (syncResult == 0)
                xs match {
                  case Synchronized => if (isReverse) ascLongOrd.compare(yl, xl) else ascLongOrd.compare(xl, yl)
                  case _ => if (isReverse) descLongOrd.compare(yl, xl) else descLongOrd.compare(xl, yl)
                }
              else syncResult
          }
        }

        override def compare(x: Option[(StatusType, Long)], y: Option[(StatusType, Long)]): Int =
          doComp(x, y, false)

        override val reverse: Ordering[Option[(StatusType, Long)]] =
          new Ordering[Option[(StatusType, Long)]] {
            override def compare(x: Option[(StatusType, Long)], y: Option[(StatusType, Long)]): Int = {
              doComp(y, x, true)
            }
            override val reverse: Ordering[Option[(StatusType, Long)]] = outer
          }

        // The scala API docs say if you override `reverse` and change the default behavior, you
        // MUST also override `isReverseOf`
        override def isReverseOf(other: Ordering[_]): Boolean = other == this.reverse
      }

    val data: List[Option[(StatusType, Long)]] = List(
      Some(Synchronized, 101L),
      Some(OutOfSync, 123L),
      Some(OutOfSync, 999L),
      Some(Synchronized, 124L),
      Some(OutOfSync, 501L),
      Some(Synchronized, 101L),
      None,
      Some(Synchronized, 999L)
    )

    println("Sorting by default...")
    val defaultSort = data.sorted(statusTupleOrdering)
    println("\nSorting in reverse...")
    val reverseSort = data.sorted(statusTupleOrdering.reverse)

    println("MAL - DEFAULT SORT:")
    defaultSort.foreach(x => println(s"  $x"))

    println("\nMAL - REVERSE SORT")
    reverseSort.foreach(x => println(s"  $x"))

    def doSort[A](items: List[A], ordA: Ordering[A]): Unit = {
      println("doSort() by default...")
      val defaultSort = items.sorted(ordA)
      println("\ndoSort() in reverse...")
      val reverseSort = items.sorted(ordA.reverse)
      println("MAL - DEFAULT SORT:")
      defaultSort.foreach(x => println(s"  $x"))
      println("\nMAL - REVERSE SORT")
      reverseSort.foreach(x => println(s"  $x"))
    }

    doSort(data, statusTupleOrdering)
  }

}
