package org.mlamarre.sandbox.common

import cats.data.State
import cats.effect._
import cats.effect.unsafe.implicits.global
import cats.implicits._
import fs2._
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._

class WorkingWithFs2Streams extends AnyFunSuite with Matchers {

  test("Flat-mapping with Stream.empty") {
    val s1: Stream[IO, Int] = Stream.emits[IO, Int](List(1,2,3,4,5))

    // What happens if we flatMap a Stream and sometimes the function we pass in results in another
    // Stream[F,A], but other times it is an empty stream (Stream[Pure, Nothing])?
    val s2: Stream[IO, Int] = s1.flatMap{ i =>
      if (i % 2 == 0) Stream.eval(IO(i * 2))
      else Stream.empty
    }

    val result = s2.compile.toList.unsafeRunSync()

    // The pure stream was merged with our effectful one without issue.
    result shouldBe List(4, 8)
  }

  test("Coalescing stream items") {
    // Models a set of lines read from a file. Empty lines delineate "records" in the file, so we
    // would like to "coalesce" adjacent elements into one Stream element. That is, we'd like to
    // turn our Stream[IO, String] into a String[IO, List[String]]
    val lines = List("1000", "3000", "", "9000", "1000", "2500", "", "4000", "", "10000", "7250")
    val s1: Stream[IO, String] = Stream.emits[IO, String](lines)
    val s2: Stream[IO, List[Int]] = ???

    // Question 1: Is there a way to coalesce adjacent stream elements cleanly, or am I going to
    // have to do some combination of takeWhile + drop to construct a new stream?
    // Answer from Mike P: "yeah, the most natural way to do this is with a custom pull"

    // Question 2: What is the guidance on "keeping things in a Stream context instead of IO/F?"
    // Like, I can easily just toList my Stream of strings above and do all of my coalescing in the
    // context of an IO, but is that bad? Or rather: when is that ok vs awful? Seems like it would
    // be a function of whether your input is finite or not and if so, how large is it? You wouldn't
    // want to read an encyclopedia's worth of strings into memory inside of an IO if you can
    // process them in a Stream, right?
    // Answer from Mike P: "yeah, generally try to stay in stream as long as possible"
    // "in a perfect world, your data sources (e.g. database apis) and sinks (e.g. web server apis) are all stream-enabled and you can do everything in finite memory"
    // "even when working with huge data sets"

    val firstItems = s1.takeWhile(!_.isBlank())

    println(s"MAL - firstItems: ${firstItems.compile.toList.unsafeRunSync()}")
    println(s"MAL - s1 after takeWhile: ${s1.take(4L).compile.toList.unsafeRunSync()}")

  }

}