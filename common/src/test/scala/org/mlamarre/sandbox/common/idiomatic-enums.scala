package org.mlamarre.sandbox.common

// This file illustrates a couple of simple examples of how to implement idiomatic scala
// enumerations in scala 2.x. These examples are based largely on this blog post:
// http://underscore.io/blog/posts/2014/09/03/enumerations.html
// The planets part of that post is in turn based off of this Java Planets enum example:
// https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html

// Bare minimum of how to declare an enum
sealed trait Weekday {}
object Weekday {
  case object Monday extends Weekday
  case object Tuesday extends Weekday
  case object Wednesday extends Weekday
  case object Thursday extends Weekday
  case object Friday extends Weekday
  case object Saturday extends Weekday
  case object Sunday extends Weekday
}

// A more realistic implementation that demonstrates some of the common helper methods that one
// would frequently include in the companion object. Whether or not your enumeration values
// themselves will included child values and methods will be a case-by-case decision, but companion
// things like `all` and fromString() are very common.
sealed abstract class Planet(
  val orderFromSun: Int,
  val name: String,
  val mass: Double,
  val radius: Double
) extends Ordered[Planet] {

  def compare(that: Planet) = this.orderFromSun - that.orderFromSun

  lazy val surfaceGravity = Planet.G * mass / (radius * radius)

  def surfaceWeight(otherMass: Double) = otherMass * surfaceGravity

  override def toString = name
}

object Planet {
  case object Mercury extends Planet(1, "Mercury", 3.303e+23, 2.4397e6)
  case object Venus extends Planet(2, "Venus", 4.869e+24, 6.0518e6)
  case object Earth extends Planet(3, "Earth", 5.976e+24, 6.3781e6)
  case object Mars extends Planet(4, "Mars", 6.421e+23, 3.3972e6)
  case object Jupiter extends Planet(5, "Jupiter", 1.9e+27, 7.1492e7)
  case object Saturn extends Planet(6, "Saturn", 5.688e+26, 6.0268e7)
  case object Uranus extends Planet(7, "Uranus", 8.686e+25, 2.5559e7)
  case object Neptune extends Planet(8, "Neptune", 1.024e+26, 2.4746e7)

  val all: Set[Planet] = Set(Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune)

  private val G = 6.67300e-11 // universal gravitational constant  (m3 kg-1 s-2)

  def fromString(value: String): Either[String, Planet] = value.toLowerCase() match {
    case "mercury" => Right(Mercury)
    case "venus"   => Right(Venus)
    case "earth"   => Right(Earth)
    case "mars"    => Right(Mars)
    case "jupiter" => Right(Jupiter)
    case "saturn"  => Right(Saturn)
    case "uranus"  => Right(Uranus)
    case "neptune" => Right(Neptune)
    case other =>
      Left(s"Unrecognized planet: $other. Valid planets are: ${all.mkString(",")}")
  }

  // TODO: Circe encoder/decoder
  // implicit val encoder: Encoder[Planet] =
  //   Encoder[Planet](p => Json.fromString(p.toString()))
  // implicit val decoder: Decoder[Planet] =
  //   Decoder[Planet](c =>
  //     c.as[String].flatMap(fromString(_).leftMap(err => DecodingFailure(err, c.history)))
  //   )
}
