package org.mlamarre

import cats.{Eq, Show}
import io.circe.{Decoder, Encoder}

/**
 * @param streetNumber
 * @param streetName
 * @param apartmentId
 * @param city
 * @param state
 * @param postalCode
 */
final case class USAAddress(
  streetNumber: Int,
  streetName: String,
  apartmentId: Option[String],
  city: String,
  state: USAState,
  postalCode: String
)

object USAAddress {
  implicit val eq: Eq[USAAddress] = Eq.fromUniversalEquals[USAAddress]
  implicit val show: Show[USAAddress] = Show.show { addr =>
    val aptId = addr.apartmentId.map(id => s" $id,").getOrElse("")
    s"${addr.streetNumber} ${addr.streetName},$aptId ${addr.city}, ${addr.state} ${addr.postalCode}"
  }

  implicit val encoder: Encoder[USAAddress] =
    Encoder.forProduct6("streetNumber", "streetName", "aptId", "city", "state", "postalCode") { a =>
      (a.streetNumber, a.streetName, a.apartmentId, a.city, a.state, a.postalCode)
    }

  implicit val decoder: Decoder[USAAddress] = Decoder.instance[USAAddress] { cur =>
    for {
      streetNumber <- cur.downField("streetNumber").as[Int]
      streetName <- cur.downField("streetName").as[String]
      aptId <- cur.downField("aptId").as[Option[String]]
      city <- cur.downField("city").as[String]
      state <- cur.downField("state").as[USAState]
      zipCode <- cur.downField("postalCode").as[String]
    } yield USAAddress(streetNumber, streetName, aptId, city, state, zipCode)
  }
}
