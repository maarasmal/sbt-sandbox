package org.mlamarre

import java.time.LocalDate

import cats.{Eq, Show}
import io.circe.{Decoder, Encoder}

/**
 * A simplistic model of a person record.
 *
 * @param firstName
 * @param lastName
 * @param address
 */
final case class Person(
  firstName: String,
  lastName: String,
  address: USAAddress,
  birthDate: LocalDate
)

object Person {
  implicit val eq: Eq[Person] = Eq.fromUniversalEquals[Person]
  implicit val show: Show[Person] = Show.show(p => s"${p.firstName} ${p.lastName}")

  implicit val encoder: Encoder[Person] =
    Encoder.forProduct4("firstName", "lastName", "address", "birthDate") { p =>
      (p.firstName, p.lastName, p.address, p.birthDate)
    }

  implicit val decoder: Decoder[Person] = Decoder.instance[Person] { cur =>
    for {
      fn <- cur.downField("firstName").as[String]
      ln <- cur.downField("lastName").as[String]
      addr <- cur.downField("address").as[USAAddress]
      bd <- cur.downField("birthDate").as[LocalDate]
    } yield Person(fn, ln, addr, bd)
  }
}
