package org.mlamarre

import cats.{Eq, Show}
import cats.syntax.all._
import io.circe._

sealed abstract class USAState(val name: String, val abbrev: String) {}

// https://en.wikipedia.org/wiki/List_of_U.S._state_and_territory_abbreviations#Table
object USAState {
  case object Alabama extends USAState("Alabama", "AL")
  case object Alaska extends USAState("Alaska", "AK")
  case object Arizona extends USAState("Arizona", "AZ")
  case object Arkansas extends USAState("Arkansa", "AR")
  case object California extends USAState("California", "CA")
  case object Colorado extends USAState("Colorado", "CO")
  case object Connecticut extends USAState("Connecticut", "CT")
  case object Delaware extends USAState("Delaware", "DE")
  case object Florida extends USAState("Florida", "FL")
  case object Georgia extends USAState("Georgia", "GA")
  case object Hawaii extends USAState("Hawai'i", "HI")
  case object Idaho extends USAState("Idaho", "ID")
  case object Illinois extends USAState("Illinois", "IL")
  case object Indiana extends USAState("Indiana", "IN")
  case object Iowa extends USAState("Iowa", "IA")
  case object Kansas extends USAState("Kansas", "KS")
  case object Kentucky extends USAState("Kentucky", "KY")
  case object Louisiana extends USAState("Louisiana", "LA")
  case object Maine extends USAState("Maine", "ME")
  case object Maryland extends USAState("Maryland", "MD")
  case object Massachusetts extends USAState("Massachusetts", "MA")
  case object Michigan extends USAState("Michigan", "MI")
  case object Minnesota extends USAState("Minnesota", "MN")
  case object Mississippi extends USAState("Mississippi", "MS")
  case object Missouri extends USAState("Missouri", "MO")
  case object Montana extends USAState("Montana", "MT")
  case object Nebraska extends USAState("Nebraska", "NE")
  case object Nevada extends USAState("Nevada", "NV")
  case object NewHampshire extends USAState("New Hampshire", "NH")
  case object NewJersey extends USAState("New Jersey", "NJ")
  case object NewMexico extends USAState("New Mexico", "NM")
  case object NewYork extends USAState("New York", "NY")
  case object NorthCarolina extends USAState("North Carolina", "NC")
  case object NorthDakota extends USAState("North Dakota", "ND")
  case object Ohio extends USAState("Ohio", "OH")
  case object Oklahoma extends USAState("Oklahoma", "OK")
  case object Oregon extends USAState("Oregon", "OR")
  case object Pennsylvania extends USAState("Pennsylvania", "PA")
  case object RhodeIsland extends USAState("Rhode Island", "RI")
  case object SouthCarolina extends USAState("South Carolina", "SC")
  case object SouthDakota extends USAState("South Dakota", "SD")
  case object Tennessee extends USAState("Tennessee", "TN")
  case object Texas extends USAState("Texas", "TX")
  case object Utah extends USAState("Utah", "UT")
  case object Vermont extends USAState("Vermont", "VT")
  case object Virginia extends USAState("Virginia", "VA")
  case object Washington extends USAState("Washington", "WA")
  case object WestVirginia extends USAState("West Virginia", "WV")
  case object Wisconsin extends USAState("Wisconsin", "WI")
  case object Wyoming extends USAState("Wyoming", "WY")

  // format: off
  val all: Set[USAState] = Set(
    Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware, Florida,
    Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana, Maine, Maryland,
    Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana, Nebraska, Nevada,
    NewHampshire, NewJersey, NewMexico, NewYork, NorthCarolina, NorthDakota, Ohio, Oklahoma,
    Oregon, Pennsylvania, RhodeIsland, SouthCarolina, SouthDakota, Tennessee, Texas, Utah, Vermont,
    Virginia, Washington, WestVirginia, Wisconsin, Wyoming)
  // format: on

  def lookupByName(s: String): Either[String, USAState] =
    all.find(_.name.toLowerCase() == s.toLowerCase()).toRight(s"Invalid state name: $s")

  def lookupByAbbrev(s: String): Either[String, USAState] =
    all.find(_.abbrev.toLowerCase() == s.toLowerCase()).toRight(s"Invalid state abbreviation: $s")

  implicit val eq: Eq[USAState] = Eq.fromUniversalEquals[USAState]
  implicit val show: Show[USAState] = Show.show(_.name)

  implicit val encoder: Encoder[USAState] =
    Encoder.forProduct2("name", "abbrev")(s => (s.name, s.abbrev))

  implicit val decoder: Decoder[USAState] =
    Decoder.instance { cur =>
      for {
        name <- cur.downField("name").as[String]
        abbrev <- cur.downField("abbrev").as[String]
        // We don't want to just do 'new USAState(name, abbrev)', since the JSON could have
        // something like { name: Iowa, abbrev: CA }. So we do a lookup for both fields and make
        // sure they resolve to the same value. blah
        nameLookup <- lookupByName(name).leftMap(err => DecodingFailure(err, cur.history))
        abbrevLookup <- lookupByAbbrev(abbrev).leftMap(err => DecodingFailure(err, cur.history))
        result <-
          if (nameLookup == abbrevLookup) Right(nameLookup)
          else Left(DecodingFailure(s"Unknown state: $name ($abbrev)", cur.history))
      } yield result
    }
}
