
// Idiomatic way to define a simple type w/ a private ctor and a companion object that does
// validation in its apply() method. This is SUPER simplistic, but can also be done with
// things like Validated, etc.
final case class Id private (value: Int)

object Id {
  def apply(value: Int): Either[String, Int] = if (value < 1) Left("Invalid") else Right(value)
}

