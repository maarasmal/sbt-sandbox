package org.mlamarre.sandbox
package learningscalacheck

import org.scalatest._
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should._
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

/**
 * Demonstrates some basic uses of the Scalacheck library in testing.
 *
 * NOTE: This uses Scalacheck from the ScalaTest library. There *are* some slight differences
 * // between how some parts of this function versus the stand-alone Scalacheck.
 */
class ScalacheckTests extends AnyFunSuite with Matchers with ScalaCheckPropertyChecks {

  test("int addition") {
    // forAll takes a function of arbitrary arity that returns Unit and will throw an
    // exception if a test fails. In this case, x and y are arbitrarily generated values
    // that Scalacheck provides for us.
    forAll { (x: Int, y: Int) =>
      x + y - y shouldBe x
    }
  }

  test("list.reverse") {
    // Again, we use a standard generator from scalacheck to dynamically generate a random list of
    // integer values.
    forAll { (xs: List[Int]) => xs.reverse.reverse shouldBe xs }
  }

  // What happens when we want to test with a class that Scalacheck doesn't know about and thus
  // cannot provide a Gen[T] for?
  case class Version(major: Int, minor: Int) extends Ordered[Version] {
    def compare(that: Version) =
      // Ordering[(Int, Int)].compare((this.major, this.minor), (that.major, that.minor))
      // An Ordering is always available for a given tuple if there is an Ordering defined for
      // each type that the tuple consists of. The commented-out line above is fine when you only
      // have 2 fields in your case class, but this form is generally more useful:
      Ordering[(Int, Int)].compare(Version.unapply(this).get, Version.unapply(that).get)

  }

  test("version comparison - without scalacheck") {
    Version(0, 0) should be < (Version(1, 0))
    Version(0, 0) should be < (Version(0, 1))
    // Repeat by hand for a bit, until we've satisfied ourselves that we've covered the necessary
    // cases. But this doesn't PROVE that our compare function works. And it relies on the
    // developer to come up w/ good examples to test the property.
  }

  // So lets use scalacheck. But it doesn't know about our case class Version, so it cannot generate
  // arbitrary instances of Version. So we have to help it out and tell it how.

  // We can of course ask Scalacheck to generate us the constituent fields of a Version, although
  // this is of somewhat limited use. Still, this isn't terrible:
  test("version construction") {
    forAll { (major: Int, minor: Int) =>
      Version(major, minor).major shouldBe major
      Version(major, minor).minor shouldBe minor
    }
  }

  // But it demonstrates that if we had an arbitrary major and minor number, we could generate an
  // arbitrary Version. So lets define that.
  import org.scalacheck.{ Arbitrary, Gen }
  import Arbitrary.arbitrary

  // Arbitrary.apply() takes a Generator, which we will build with a for comprehension. Note the use
  // of the arbitrary() function, which returns a generator.
  implicit val arbVersion: Arbitrary[Version] = Arbitrary(for {
    major <- arbitrary[Int]
    minor <- arbitrary[Int]
  } yield Version(major, minor))

  test("version comparison - with scalacheck") {
    forAll { (v: Version) => v compare v shouldBe 0 }

    // This property will fail when v.minor is Int.MAX_VALUE, so adding 1 causes integer overflow,
    // making the second minor value to be negative, thus ruining our comparison.
    // forAll{ (v: Version) => v compare Version(v.major, v.minor + 1) shouldBe - 1 }

    // We could ask for TWO arbitrary Versions, but then we don't really know anything about
    // those two values, so comparing them is impossible.
  }

  // So we define an Arbitrary instance that generates us TUPLES of values that we define to
  // be created subject to the constraints that we want to test.
  // implicit val arbAscendingVersionTuple: Arbitrary[(Version, Version)] = Arbitrary( for {
  //   major1 <- Gen.choose(0, 100)
  //   minor1 <- Gen.choose(0, 100)
  //   major2 <- Gen.choose(major1, 101)
  //   minor2 <- Gen.choose(minor1, 101)
  // } yield (Version(major1, minor1), Version(major2, minor2))

  // However, it is a bit weird and unlikely that someone would want to generate such arbitrary
  // tuples. So this makes a bit more sense:
  val ascendingVersionTuple: Gen[(Version, Version)] = for {
    major1 <- Gen.choose(0, 100)
    minor1 <- Gen.choose(0, 100)
    major2 <- Gen.choose(major1, 101)
    minor2 <- Gen.choose(minor1, 101)
  } yield (Version(major1, minor1), Version(major2, minor2))

  // The only real difference between Arbitrary and Gen is that Arbitrary is a "marker" that
  // wraps generators and allows them to be found by implicit lookup.

  // So our arbVersion above makes a lot of sense, as generating Versions is likely something
  // that any test might want. But for things that are going to be specific to one or a small
  // handful of tests, leaving it as an unwrapped Gen makes more sense. The test can always
  // locally wrap the Gen in an Arbitrary if it wants that kind of behavior, although it isn't
  // really necessary to do so.

  test("version comparison - using tuple generator") {
    // We can now pass our tuple generator directly to forAll:
    forAll(ascendingVersionTuple) { case (u, v) => u compare v should (be(-1) or be(0)) }
  }

  // GeneratorDrivenPropertyChecks provides a couple different ways to customize test behavior.
  // We can pass configuration to forAll(), or we can put implicits in scope. So for example,
  // to generate 1000 test cases instead of 100:
  test("Controlling the number of attempts for all tests") {
    // Normally you'd put this outside of the test method, as an "implicit override val",
    // but we don't actually want to change the default for the rest of the tests here.
    implicit val generatorDrivenConfig: PropertyCheckConfiguration =
      PropertyCheckConfiguration(minSuccessful = 1000)
    var numRuns = 0
    forAll { n: Int =>
      numRuns = numRuns + 1
      n - n should be(0)
    }
    numRuns should be(1000)
  }

  // You can also explicitly configure minSuccessful for each test:
  test("Controlling the number of attempts for a single test") {
    var numRuns = 0
    forAll(minSuccessful(231)) { n: Int =>
      numRuns = numRuns + 1
      n - n should be(0)
    }
    numRuns should be(231)
  }

  // There is a similar trait called Checkers. The main difference is that instead of using
  // matchers, you have to return a boolean. Generally when writing your own properties, you
  // will want to use GeneratorDrivenPropertyChecks.

  // Rather than creating our own weird generators, there is another syntax that lets us
  // establish that we want our generated values to adhere to some constraints. Mike showed
  // a test class for a VersionRange, which is built from two Versions. One of the tests
  // looked like this:
  // forAll { (a: Version, b: Version) =>
  //   whenever (a < b) {
  //     // Property checks went in here
  //   }
  // }

  test("Using 'whenever' to limit the generated inputs") {
    forAll { (n: Int) =>
      whenever(n > 1) {
        n / 2 should be > 0
      }
    }
  }

  test("Passing generators to forAll") {
    // Similar to above, but without using 'whenever'. The Gen object predefines a number of
    // standard generators for common pieces of data like strings, alphabetical characters,
    // positive numbers, etc. Also a number of combinators for building Gen's from other
    // Gen's (such as Gen.option, which takes a Gen[T] and returns you a Gen[Option[T]])
    forAll(Gen.posNum[Long]) { n: Long =>
      scala.math.abs(n) should be >= 0L
    }
  }

}
